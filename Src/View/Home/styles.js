import React from "react";
import { Platform } from "react-native";
import {StyleSheet} from "react-native";
import { getBottomSpace, getStatusBarHeight } from 'react-native-iphone-x-helper'
import { RFValue } from "react-native-responsive-fontsize";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { largeText, mediumText, regularText, smallText, xlText } from "../../Constants/AppStyle";
import { grey, HeadingColor ,profileBGColor,profileColor,subHeadingColor } from "../../Constants/Colors";
import { Bold, Medium, Regular } from "../../Constants/Fonts";

const styles = StyleSheet.create({
    container:{
        // marginTop:getStatusBarHeight() + 30,
        flex:1,
        backgroundColor:profileBGColor,

    },
    headerOuter:{
        marginHorizontal: 20,
        flexDirection: 'row',
         alignItems: 'center',
         justifyContent:'space-between',
    },
    header:{
        color:HeadingColor,
        fontFamily:Bold,
        fontSize:largeText,
        marginTop:Platform.OS=='android'?getStatusBarHeight(): getStatusBarHeight() + 30,
    },
    content:{
        marginHorizontal:15,
        flex:1,
        marginBottom:getBottomSpace()+10,
        marginTop:20,
    },
    inMiddle:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    }
})
export default styles
