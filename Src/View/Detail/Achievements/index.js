import React, { useEffect, useReducer, useRef } from 'react'
import { View, Text, TouchableOpacity, Image, TextInput } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Icon, Divider } from 'react-native-elements'
import moment from 'moment'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';


import InputField from '../../../Component/InputField';
import DateInput from '../../../Component/DateInput';
import styles from './styles'
import LGCircle from '../../../Component/LGCircle'
import { HeadingColor } from '../../../Constants/Colors'
import { widthPercentageToDP } from 'react-native-responsive-screen'
import _DatePicker from '../../../Component/_DatePicker';
import AppButton from '../../../Component/AppButton'
import { medal } from '../../../Constants/ConstantValues'

function Achievements({ navigation }) {

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            achievement1: undefined,
            achievement2: undefined,
            achievement3: undefined,
        }
    )
    const {
        achievement1,
        achievement2,
        achievement3,

    } = state



    function handleNext() {navigation.navigate('Skills')}



    function renderAchievements() {
        return (
            <View>
                <View style={styles.contentView}>

                    <InputField
                        placeholder={'Achievement 1'}
                        image={medal}
                        onChangeText={(text) => updateState({ achievement1: text })}
                        inputContainerStyle={{ ...styles.inputContainerStyle, marginTop: 20 }}
                    />

                    <InputField
                        placeholder={'Achievement 2'}
                        image={medal}
                        onChangeText={(text) => updateState({ achievement2: text })}
                        inputContainerStyle={{ ...styles.inputContainerStyle }}
                    />
                    <InputField
                        placeholder={'Achievement 3'}
                        image={medal}
                        onChangeText={(text) => updateState({ achievement3: text })}
                        inputContainerStyle={{ ...styles.inputContainerStyle }}
                    />

                </View>
                <AppButton
                    title={'NEXT'}
                    onPress={handleNext}
                    gradientBtnStyle={{ marginTop: 10, width: wp(50) }}
                />
            </View>
        )
    }



    return (
        <>
            <View style={styles.container}>
                <KeyboardAwareScrollView>
                    <View style={{ marginHorizontal: 20 }}>
                        <Text style={styles.header}>Let`s Complete your Profile </Text>
                        <Text style={styles.subHeading}>Please fill in the Details Below</Text>
                    </View>
                    <Text style={styles.stepHeading}>Step 3</Text>

                    <View style={styles.stepsHeadingView}>

                        <Text style={styles.stepText}>Education</Text>
                        <Text style={styles.stepText}>Experience</Text>
                        <Text style={styles.stepText}>Achievements</Text>
                        <Text style={styles.stepText}>Skills</Text>
                        <Text style={styles.stepText}>Hobbies</Text>
                    </View>

                    <View style={{ alignItems: 'center' }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>

                            <LGCircle gradientBtnStyle={{ ...styles.gradientBtnStyle, left: 10 }} />
                            <Divider style={{ left: 11, borderColor: HeadingColor, borderWidth: 2, width: widthPercentageToDP((100 / 5) - 7) }} />

                            <LGCircle gradientBtnStyle={{ ...styles.gradientBtnStyle, left: 12 }} />
                            <Divider style={{ left: 14, borderColor: HeadingColor, borderWidth: 2, width: widthPercentageToDP((100 / 5) - 5) }} />

                            <LGCircle gradientBtnStyle={{ ...styles.gradientBtnStyle, left: 10 }} />
                            <Divider style={{ left: 12, borderColor: HeadingColor, borderWidth: 2, width: widthPercentageToDP((100 / 5) - 7) }} />

                            <LGCircle gradientBtnStyle={{ ...styles.gradientBtnStyle, left: 12 }} />
                            <Divider style={{ borderColor: HeadingColor, borderWidth: 2, width: widthPercentageToDP((100 / 5) - 7) }} />

                            <LGCircle gradientBtnStyle={{ ...styles.gradientBtnStyle }} />

                        </View>
                    </View>

                    {renderAchievements()}

                </KeyboardAwareScrollView>
                <_DatePicker
                />
                {/* <_DatePicker
                    date={startDate}
                    isVisible={isStartDateVisible}
                    onDateChange={selectDate}
                    onSelect={onSelectStartDate}
                    onClose={hideDatePicker}
                />
                <_DatePicker
                    date={endDate}
                    isVisible={isEndDateVisible}
                    onDateChange={selectEndDate}
                    onSelect={onSelectEndDate}
                    onClose={hideEndDatePicker}
                /> */}
            </View>
        </>
    )
}
export default Achievements
