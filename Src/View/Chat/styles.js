import React from "react";
import { Platform, StyleSheet } from "react-native";
import { getStatusBarHeight, getBottomSpace } from 'react-native-iphone-x-helper'
import { RFValue } from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { largeText, mediumText, regularText, smallText, xlText } from "../../Constants/AppStyle";
import { black, grey, HeadingColor, profileBGColor, profileColor, subHeadingColor } from "../../Constants/Colors";
import { Bold, Medium, Regular } from "../../Constants/Fonts";


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'

    },
    header: {
        color: black,
        fontFamily: Medium,
        fontSize: mediumText,
        marginTop:Platform.OS =='android'? getStatusBarHeight(): getStatusBarHeight() + 30,
        textAlign: 'center'

    },
    flatListStyle: {
        marginHorizontal: 20,
    },
    inputStyle: {
        // padding: Platform.OS === 'android' ? 10 : 10,
        paddingHorizontal: 5,
        paddingVertical: 10,
        flex: 1,
        color: black,
        fontFamily: Regular,
        marginTop: 10,
        // borderRadius:5,
        // backgroundColor:'green',
        // elevation:1,
        // shadowColor:'#000',
        // shadowOffset:{height:2,width:0},
        // shadowRadius:5,
        // shadowOpacity:0.2,
    },

    innerView: {
        flex: 1,
        flexDirection: 'row',
        // alignItems:'center',
        paddingLeft: 5,
        borderRadius: 10,
        backgroundColor: profileBGColor,
        elevation: 2,
        shadowColor: '#000',
        shadowOffset: { height: 2, width: 0 },
        shadowRadius: 5,
        shadowOpacity: 0.2,
    },
    recvMsg: {
        borderRadius: 20,
        paddingHorizontal: 15,
        paddingVertical: 10,
        backgroundColor: profileBGColor,
        marginBottom: 10,
        width: wp(70),
        alignSelf: "flex-start"
    },
    sendMsg: {
        borderRadius: 20,
        paddingHorizontal: 15,
        paddingVertical: 10,
        backgroundColor: HeadingColor,
        marginBottom: 10,
        width: wp(70),
        alignSelf: "flex-end"
    },
    title: {
        textAlign: 'center',
        marginBottom: 10,
        marginTop: 10,
    },
    sendMsgText: {
        color: 'white',
        fontFamily: Regular,
        fontSize: regularText,
    },
    recvMsgText: {
        color: black,
        fontFamily: Regular,
        fontSize: regularText,
    },
    outerView: {
        flexDirection: 'row',
        backgroundColor: 'white',
        maxHeight: 100,
        marginBottom: getBottomSpace()
    },
    iconContainerStyle: {
        alignSelf: 'flex-end',
        marginBottom: 5
    },
    iconStyle: {
        fontSize: 30,
        color: profileColor,

    },
    sendIconStyle: {
        color: HeadingColor,
        fontSize: 30,
    },
    voiceRecorderView: {
        height: 100,
        flex: 1,
        backgroundColor: 'white',
        elevation: 2,

    },
    voiceRecorderInnerView: {
        flex: 1,
        backgroundColor: 'white',
        elevation: 2,
        shadowColor: '#000',
        shadowOffset: { height: 2, width: 0 },
        shadowRadius: 5,
        shadowOpacity: 0.2,
    },
    timerView: {
        marginHorizontal: 10,
        marginTop: 10,
    },
    actionView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginHorizontal: 20,
        marginTop:10,

    },
    deleteIcon:{
        color:profileColor,
        fontSize:30,
    },
    pauseIcon:{
        color:HeadingColor,
        fontSize:30,
    },
    playRecordView:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between' ,
        backgroundColor:profileBGColor
    },
    attachIconStyle:{
        color:profileColor,
        fontSize:25,
        marginRight:10,
    }
})
export default styles