import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import { Menu, MenuItem, MenuDivider } from 'react-native-material-menu';
import { mediumText, regularText } from '../Constants/AppStyle'
import { grey, HeadingColor, black, profileColor } from '../Constants/Colors'
import { download } from '../Constants/ConstantValues'
import { Bold, Medium, Regular } from '../Constants/Fonts'
import { Icon, Divider } from 'react-native-elements'
import moment from 'moment'
import { URL } from '../Utility/server'

const Reviews = (props) => {
    const { item, handleReport, openMenu } = props
    let userImg = item?.attachments[0]?.url
    let userName = item?.user?.first_name

    let commentDate = moment(item?.created_at).format('MMMM Do YYYY')

    function renderStart(input) {
        const star = []
        for (let i = 0; i < input; i++) {
            star.push(
                <Icon
                    name={'star'}
                    type={'font-awesome'}
                    iconStyle={{ color: HeadingColor, paddingHorizontal: 2, fontSize: 12 }}
                />
            )
        }
        return (
            <View style={{ alignSelf: 'flex-start', flexDirection: 'row' }}>{star}</View>
        )
    }

    return (
        <View>

            <View style={styles.head}>
                <View style={styles.left}>
                    <View style={styles.imgView}>
                        <Image
                            source={userImg ? { uri: `${URL}${userImg}` } : download}
                            style={styles.imgStyle}
                        />
                    </View>
                    <Text style={styles.nameStyle}>{userName}</Text>
                </View>
                <Text style={styles.date}>{`${commentDate}`}</Text>
            </View>

            <View style={{ marginLeft: 50, }}>
                <View style={{ ...styles.ratingView, justifyContent: 'space-between' }}>
                    <View style={styles.ratingView}>
                        {renderStart(5)}
                        <Text style={styles.reviewText}>{'(5.0)'}</Text>
                    </View>
                    <Icon
                        onPress={openMenu}
                        name='more-vertical'
                        type='feather'
                        iconStyle={{ fontSize: 20, color: black }}
                    />
                </View>
                
            </View> 


            <Text style={styles.textStyle}>
                {`${item?.content}`}
            </Text>
            
            <Divider style={styles.divider} />

        </View>
    )
}
export default Reviews

const styles = StyleSheet.create({
    head: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    left: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    imgView: {
        width: 40,
        height: 40,
        borderRadius: 40 / 2,
        overflow: 'hidden'
    },
    imgStyle: {
        width: undefined,
        height: undefined,
        flex: 1,
    },
    nameStyle: {
        marginLeft: 10,
        color: HeadingColor,
        fontSize: mediumText,
        fontFamily: Bold,
    },
    ratingView: {
        flexDirection: 'row',
        alignItems: 'center',
        // justifyContent:'space-between'
    },
    reviewText: {
        // marginTop:10,
        color: grey,
        fontFamily: Regular,
        fontSize: regularText,
        marginLeft: 5
    },

    textStyle: {
        color: grey,
        fontFamily: Regular,
        fontSize: 13,
        marginBottom: 10
    },
    divider: {
        borderBottomWidth: StyleSheet.hairlineWidth,
        color: profileColor,
        marginBottom: 15,
    },
    date: {
        color: grey,
        fontSize: regularText,
        fontFamily: Regular,
    }
})