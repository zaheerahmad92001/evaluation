import React, { useEffect, useReducer } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Icon } from 'react-native-elements'

import { forgot , checked } from '../../Constants/ConstantValues'
import InputField from '../../Component/InputField';
import AppButton from '../../Component/AppButton'
import styles from './styles';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

function PasswordChange({ navigation }) {
  useEffect(() => {
  }, [])
  const [state, updateState] = useReducer(
    (sate, newState) => ({ ...state, ...newState }),
    {email:undefined,}
  )
  const { email } = state

  function handleContinue(){navigation.navigate('Verified')}

  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView>

      <View style={{ marginHorizontal: 20 }}>
        <Text style={{ ...styles.header}}>Password Changed</Text>
        <Text style={styles.subHeading}>Your Password has been changed</Text>
        <Text style={{ ...styles.subHeading, marginTop: 2 }}>Successfully!</Text>
      </View>

        <View style={styles.imgBg}>
          <Image
            source={checked}
            style={styles.imgStyle}
          />
        </View>

        <View style={{...styles.contentView}}>
         
          <AppButton
            title={'CONTINUE'}
            onPress={handleContinue}
            gradientBtnStyle={{marginTop:10, width:wp(70)}}
        />
        </View>
      </KeyboardAwareScrollView>
    </View>
  )
}

export default PasswordChange
