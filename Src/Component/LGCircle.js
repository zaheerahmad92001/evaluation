import React from 'react'
import { View, Text, StyleSheet ,Image } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { ferozi, grey, HeadingColor, profileColor } from '../Constants/Colors'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Medium, Regular } from '../Constants/Fonts';
import {Icon}from 'react-native-elements'
import { TouchableOpacity } from 'react-native';
const LGCircle = (props) => {
    const { 
         iconName,iconType,
         img, 
         onPress,
         number, 
         active,
         color1,
         color2,
         numStyle,
        startX, startY , 
        endX , endY
        } = props
    return (
        <TouchableOpacity onPress={img || onPress}  activeOpacity={0.7}>
            <LinearGradient
                start={{ x: startX || 0.85, y: startY || 0.9 }} end={{ x:endX || 1, y: endY || 0.4 }}
                colors={[ color1 || profileColor, color2 || profileColor]}
                // colors={[ferozi, HeadingColor]}
                style={[styles.linearGradient, props.gradientBtnStyle]}>
                 {img ?
                 <Image
                  source={img}
                  style={props.imgStyle}
                 />
                 :iconName ?
                 <Icon 
                //   onPress={onPress}
                  name={iconName}
                  type={iconType}
                  iconStyle={props.iconStyle}
                  />:
                  <Text style={numStyle}>{number}</Text>
                 }
            </LinearGradient>
        </TouchableOpacity>
    )
}
export default LGCircle

const styles = StyleSheet.create({
    linearGradient: {
        // flex: 1,
        // paddingLeft: 15,
        // paddingRight: 15,
        // alignSelf: 'center',
        // width: wp(60),
        width:20,
        height:20,
        borderRadius: 20/2,
        justifyContent:'center',
        alignItems:'center'

    },
    buttonText: {
        fontSize: 18,
        fontFamily: Regular,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        backgroundColor: 'transparent',
    },
})