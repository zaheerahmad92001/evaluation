import React from "react";
import { Platform } from "react-native";
import {StyleSheet} from "react-native";
import { getStatusBarHeight } from 'react-native-iphone-x-helper'
import { RFValue } from "react-native-responsive-fontsize";
import { heightPercentageToDP  as hp} from "react-native-responsive-screen";
import { imgBGHeight, imgHeight, largeText, mediumText, regularText, smallText, xlText } from "../../Constants/AppStyle";
import { grey, HeadingColor ,lightRose,profileBGColor,profileColor,subHeadingColor } from "../../Constants/Colors";
import { Bold, Medium, Regular } from "../../Constants/Fonts";


const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white' 

    },
    header:{
        color:HeadingColor,
        fontFamily:Bold,
        fontSize:largeText,
        marginTop:Platform.OS=='android'?getStatusBarHeight(): getStatusBarHeight() + 30,


    },
    subHeading:{
        color:subHeadingColor ,
        fontSize:mediumText,
        fontFamily:Regular,
        marginTop:RFValue(5)

    },
    
    imgBg:{
        width:imgBGHeight,
        height:imgBGHeight,
        borderRadius:imgBGHeight/2,
        backgroundColor:lightRose,
        alignItems:'center',
        justifyContent:'center',
        alignSelf:'center',
        marginVertical:hp(5),
    },

    imgStyle:{
        width:imgHeight,
        height:imgHeight,
    },
    contentView:{
        marginHorizontal:35,
    },

    account:{
        marginTop:25,
        alignItems:'center',
        justifyContent:'center',
        flexDirection:'row'
    },
    leftText:{
        color:grey,
        fontFamily:Regular,
        fontSize:regularText
    },
    loginBtn:{
        marginLeft:3,
        fontSize:regularText,
        fontFamily:Bold,
        color:HeadingColor,
    },
    
    
   


})
export default styles