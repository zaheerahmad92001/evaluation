import React, { useEffect, useReducer } from 'react'
import { View, Text, TouchableOpacity, ActivityIndicator, } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Icon } from 'react-native-elements'

import InputField from '../../Component/InputField';
import AppButton from '../../Component/AppButton'
import styles from './styles';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import AsyncStorage from "@react-native-async-storage/async-storage";



import { LoginUser } from '../../Utility/AuthAPi'
import { ValidateEmail } from '../../Utility/RandomFun';
import { ferozi } from '../../Constants/Colors'
import {USER}from '../../Constants/ConstantValues'



function Login({ navigation }) {

  useEffect(() => {

  }, [])

  const [state, updateState] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
      securePass: true,
      name: '',
      email: '',
      password: '',
      emailError: '',
      passwordError: '',
      inprocess: false,
    }
  )
  const {
    securePass,
    email,
    password,
    name,
    emailError,
    passwordError,
    inprocess
  } = state


  function hideShowPassword() {
    updateState({ securePass: !securePass })
  }

  function handleSignUp() { navigation.navigate('SignUp') }

  async function handleLogin() {

    let error = false
    if (!ValidateEmail(email)) {
      updateState({ emailError: 'Please check your credentials' })
      error = true

    }
    if (!password || password?.length == 0 || password == '') {
      updateState({ passwordError: 'Please check your credentials' })
      error = true

    }


    if (!error) {
      updateState({ inprocess: true })
      let obj = {
        email: email,
        password: password
      }

      LoginUser(obj).then(async (response) => {
        if (response.api_status) {
          await AsyncStorage.setItem(USER, JSON.stringify(response?.user))
          updateState({ inprocess: false })
          navigation.goBack()
          // navigation.navigate('Detail')
        } else {
          console.log('error ', response)
          alert('please check your credentials')
          updateState({ inprocess: false })

        }
      }).catch((error) => {
        console.log('error', error)
        updateState({ inprocess: false })

      })
    }
  }

  function handleForgotPassword() { navigation.navigate('ForgotPassword') }

  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView>

        <View style={{ marginHorizontal: 20 }}>
          <Text style={styles.header}>Log in</Text>
          <Text style={styles.subHeading}>Log in to your account</Text>
        </View>


        <View style={styles.contentView}>

          <InputField
            placeholder={'Email'}
            iconName={'email'}
            iconType={'material'}
            onChangeText={(text) => updateState({ email: text, emailError: '' })}
            value={email}
            inputContainerStyle={styles.inputContainerStyle}
          />
          {emailError ?
            <Text style={styles.errorText}>{emailError}</Text> : null}

          <InputField
            placeholder={'Password'}
            iconName={'locked'}
            iconType={'fontisto'}
            passIconType={'entypo'}
            onChangeText={(text) => updateState({ password: text, passwordError: '' })}
            value={password}
            secureTextEntry={securePass}
            hideShowPassword={hideShowPassword}
            passIconName={securePass ? 'eye-with-line' : 'eye'}
            inputContainerStyle={{ ...styles.inputContainerStyle }}
          />
          {passwordError ?
            <Text style={styles.errorText}>{passwordError}</Text> : null}

          {/* <TouchableOpacity onPress={handleForgotPassword}>
            <View style={styles.forgotPassView}>
              <Text style={styles.forgotText}>Forgot Password?</Text>
            </View>
          </TouchableOpacity> */}

        </View>
        {inprocess ?
          <ActivityIndicator
            color={ferozi}
            size={'small'}
          />
          :
          <AppButton
            title={'LOGIN'}
            inprocess={inprocess}
            onPress={handleLogin}
            gradientBtnStyle={{ marginTop: 40, width: wp(70),paddingVertical:10 }}
          />
        }
        <TouchableOpacity onPress={handleSignUp}>
          <View style={styles.account}>
            <Text style={styles.leftText}>Do not have an account?</Text>
            <Text style={styles.loginBtn}>Sign up</Text>
          </View>
        </TouchableOpacity>

        {/* <Text style={styles.ORText}>OR</Text>
        <Text style={styles.otherMethod}>Sing in using</Text>

        <View style={styles.socialLoginView}>
          <View style={styles.btnBG}>
            <Icon name={'google'} type={'fontisto'} iconStyle={styles.iconStyle} />
          </View>
          <View style={styles.btnBG}>
            <Icon name={'facebook'} type={'fontisto'} iconStyle={styles.iconStyle} />
          </View>
          <View style={styles.btnBG}>
            <Icon name={'apple'} type={'fontisto'} iconStyle={styles.iconStyle} />
          </View>
        </View> */}
      </KeyboardAwareScrollView>

    </View>
  )
}
export default Login