
import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Platform } from 'react-native'
import DatePicker from 'react-native-date-picker';
import { RFValue } from 'react-native-responsive-fontsize';
import { Overlay } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient'


import { mediumText, regularText } from '../Constants/AppStyle';
import { ferozi, HeadingColor } from '../Constants/Colors';
import { Icon } from 'react-native-elements';



export const _DatePicker = (props) => {
    const { isVisible, maxDate } = props
    return (
        <Overlay
            isVisible={isVisible ? isVisible : false}
            useNativeDriver={true}
            overlayStyle={styles.overlayStyle}
        >
            <View style={styles.wraper}>
                {/* <View style={styles.header}> */}
                <LinearGradient
                    start={{ x: 0, y: 0.5 }} end={{ x: 1, y: 1 }}
                    colors={[ferozi, HeadingColor]}
                    style={[styles.header, props.gradientBtnStyle]}>

                    <Text style={styles.textStyle}>{props.header}</Text>
                    <Icon
                        onPress={props.onClose}
                        name={props.iconName ? props.iconName : 'close'}
                        type={props.iconType ? props.iconType : 'fontisto'}
                        iconStyle={styles.iconStyle}
                    />
                </LinearGradient>
                {/* </View> */}

                <DatePicker
                    date={props?.date ?? new Date()}
                    onDateChange={props.onDateChange}
                    mode="date"
                    style={{ position: 'relative', marginVertical: 20 }}
                    minimumDate={props?.minimumDate ? new Date(props.minimumDate) : null}
                    maximumDate={maxDate ? maxDate : null}
                />
                <LinearGradient
                    start={{ x: 0, y: 0.5 }} end={{ x: 1, y: 1 }}
                    colors={[ferozi, HeadingColor]}
                    style={[styles.linearGradient, props.gradientBtnStyle]}>
                    <TouchableOpacity
                        activeOpacity={1}
                        onPress={props.onSelect}
                    // style={styles.btnView}
                    >
                        <Text style={styles.okBtnStyle}>OK</Text>
                    </TouchableOpacity>
                </LinearGradient>

            </View>

        </Overlay>
    )
}
export default _DatePicker;
const styles = StyleSheet.create({

    overlayStyle: {
        backgroundColor: 'transparent',
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        borderBottomRightRadius: 22,
        borderBottomLeftRadius: 22,
        marginLeft: 25,
        marginRight: 25,
        paddingLeft: 0,
        paddingBottom: 0,
        paddingTop: 0,
        paddingRight: 0,
    },

    wraper: {
        backgroundColor: 'white',
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        borderBottomRightRadius: 22,
        borderBottomLeftRadius: 22,
        // marginHorizontal:25,
    },
    header: {
        backgroundColor: HeadingColor,
        height: RFValue(40),
        width: 'auto',
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
    },
    textStyle: {
        color: 'white',
        fontSize: regularText,
        marginLeft: 10,
        // fontWeight: 'bold'
    },
    titleStyle: {
        fontSize: 15,
        color: 'white'
    },
    iconStyle: {
        marginRight: 10,
        color: 'white',
        fontSize: 20,
        marginTop: 0
    },
    btnView: {
        paddingVertical: Platform.OS == 'android' ? 10 : 13,
        backgroundColor: HeadingColor,
        borderBottomEndRadius: 20,
        borderBottomLeftRadius: 20,
    },
    linearGradient: {
        paddingVertical: Platform.OS == 'android' ? 10 : 13,
        borderBottomEndRadius: 20,
        borderBottomLeftRadius: 20,
    },
    okBtnStyle: {
        fontSize: mediumText,
        marginLeft: 5,
        fontWeight: '600',
        color: 'white',
        textAlign: 'center',
    },

})