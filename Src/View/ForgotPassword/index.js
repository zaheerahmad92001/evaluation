import React, { useEffect, useReducer } from 'react'
import { View, Text, ActivityIndicator, Image } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Icon } from 'react-native-elements'

import { forgot } from '../../Constants/ConstantValues'
import InputField from '../../Component/InputField';
import AppButton from '../../Component/AppButton'
import styles from './styles';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { forgotPassword } from '../../Utility/AuthAPi';
import { ValidateEmail } from '../../Utility/RandomFun'
import { ferozi } from '../../Constants/Colors'


function ForgotPassword({ navigation }) {
  useEffect(() => {
  }, [])
  const [state, updateState] = useReducer(
    (sate, newState) => ({ ...state, ...newState }),
    {
      email: undefined,
      emailError: '',
      inProcess: false,
    }
  )
  const { email, emailError, inProcess } = state

  function handleContinue() {
    let error = false
    if (!ValidateEmail(email)) {
      error = true
      updateState({ emailError: 'Please enter valid email' })
    }
    if (!error) {
      updateState({ inProcess: true })

      let obj = {
        email: email
      }
      forgotPassword(obj).then((response) => {
        updateState({ inProcess: false })
        if(response?.api_status){
           navigation.navigate('PhoneVerification')
        }else{
          console.log('error',response)
          alert('user not found')
        }
      }).catch((error) => {
        console.log('error', error)
      })
    }
    // navigation.navigate('PhoneVerification')
  }

  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView>

        <View style={{ marginHorizontal: 20 }}>
          <Text style={styles.header}>Forgot your</Text>
          <Text style={{ ...styles.header, marginTop: 2 }}>Password?</Text>
          <Text style={styles.subHeading}>Please enter your Email Address</Text>
          <Text style={{ ...styles.subHeading, marginTop: 2 }}>to get instructions to reset</Text>
          <Text style={{ ...styles.subHeading, marginTop: 2 }}>Password</Text>
        </View>

        <View style={styles.imgBg}>
          <Image
            source={forgot}
            style={styles.imgStyle}
          />
        </View>

        <View style={{ ...styles.contentView }}>
          <InputField
            placeholder={'Email'}
            iconName={'email'}
            iconType={'material'}
            onChangeText={(text) => updateState({ email: text, emailError: '' })}
            inputContainerStyle={styles.inputContainerStyle}
          />
          {emailError ?
            <Text style={styles.errorText}>{emailError}</Text> : null}
          {inProcess ?
            <View style={{ marginTop: 20 }}>
              <ActivityIndicator
                color={ferozi}
                size="small"
              />
            </View> :
            <AppButton
              title={'CONTINUE'}
              onPress={handleContinue}
              gradientBtnStyle={{ marginTop: 30, width: wp(70) }}
            />
          }
        </View>
      </KeyboardAwareScrollView>
    </View>
  )
}

export default ForgotPassword
