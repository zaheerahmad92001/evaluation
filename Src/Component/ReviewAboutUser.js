import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { Overlay } from 'react-native-elements';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import StarRating from 'react-native-star-rating';


import LGCircle from './LGCircle';
import { review } from '../Constants/ConstantValues'
import { mediumText } from '../Constants/AppStyle';
import { grey, HeadingColor, lightYellow, profileBGColor, profileColor } from '../Constants/Colors';
import { Medium } from '../Constants/Fonts';
import InputField from './InputField';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AppButton from './AppButton';


const imgMarginBottom = 40
const imgHeight = 80
const imgWidth = 80
const topMargin = 15
const horizontalPadding = 25

const RateReview = (props) => {
    const { isVisible,
        starCount,
        onStarRatingPress,
        onChangeText,
        feedback,
        onSubmitPress
    } = props

    // console.log('isVisible', props)

    return (
        <KeyboardAwareScrollView>
            <Overlay
                isVisible={isVisible}
                useNativeDriver={true}
                animationIn={props.animationIn}
                animationInTiming={props.animationInTiming}
                animationOut={props.animationOut}
                overlayStyle={styles.overlayStyle}
            // style={{marginBottom:0}}
            >
                <View style={styles.modalStyling}>
                    <View style={styles.imgContainer}>
                        <LGCircle
                            startX={0.7}
                            img={review}
                            imgStyle={{ width: 40, height: 40 }}
                            //  iconName={'plus'}
                            //  iconType={'antdesign'}
                            //  iconStyle={{color:'white'}}
                            gradientBtnStyle={{ width: imgWidth, height: imgHeight, borderRadius: imgHeight / 2 }}
                        />
                    </View>
                    <View style={{ marginTop: -imgMarginBottom + 15 }}>
                        <Text style={styles.header}>Please rate & give a review!</Text>

                        <StarRating
                            disabled={false}
                            maxStars={5}
                            rating={starCount}
                            selectedStar={onStarRatingPress}
                            fullStarColor={lightYellow}
                            emptyStarColor={profileColor}
                            emptyStar={'star'}
                            fullStar={'star'}
                            containerStyle={{ width: '50%', marginVertical: 10, alignSelf: 'center' }}
                            starSize={25}
                        />

                        <InputField
                            placeholder={'Share Your reviews'}
                            onChangeText={onChangeText}
                            value={feedback}
                            InputStyle={styles.textArea}
                            iconStyle={{ marginTop: 3, }}
                            inputContainerStyle={{ ...styles.inputContainerStyle, alignItems: 'flex-start', paddingVertical: 5 }}
                            multiline={true}

                        />

                        <AppButton
                            title={'Submit'}
                            onPress={onSubmitPress}
                            gradientBtnStyle={{ marginTop: 10, width: wp(30) }}
                        />

                    </View>

                </View>

            </Overlay>
        </KeyboardAwareScrollView>
    )
}

export default RateReview;

const styles = StyleSheet.create({
    overlayStyle: {
        backgroundColor: 'transparent',
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        borderBottomRightRadius: 15,
        borderBottomLeftRadius: 15,
        width: wp(85),
        paddingLeft: 0,
        paddingBottom: 0,
        paddingTop: 0,
        paddingRight: 0
    },
    modalStyling: {
        backgroundColor: 'white',
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        borderBottomRightRadius: 15,
        borderBottomLeftRadius: 15,
        paddingBottom: 20,
        paddingHorizontal: 20
        // marginHorizontal: 25,
    },

    imgContainer: {
        width: imgWidth,
        height: imgHeight,
        borderRadius: imgHeight / 2,
        alignItems: 'center',
        alignSelf: 'center',
        bottom: imgMarginBottom,
    },
    contentStyling: {
        paddingHorizontal: horizontalPadding,
        marginTop: topMargin / 2,
        marginBottom: topMargin,
        justifyContent: 'center',
        alignItems: 'center',
    },
    header: {
        textAlign: 'center',
        fontFamily: Medium,
        fontSize: mediumText,
        color: HeadingColor
    },
    textArea: {
        height: 80,
        justifyContent: "flex-start",
        textAlignVertical: 'top'
    },
    inputContainerStyle: {
        marginBottom: 20,
        backgroundColor: profileBGColor,
        paddingHorizontal: 0,
        borderWidth: 0,
        borderRadius: 10,
    },

})