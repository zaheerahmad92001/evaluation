import React from "react";
import { Platform } from "react-native";
import {StyleSheet} from "react-native";
import { getStatusBarHeight } from 'react-native-iphone-x-helper'
import { RFValue } from "react-native-responsive-fontsize";
import { largeText, mediumText, regularText, smallText, xlText } from "../../Constants/AppStyle";
import { grey, HeadingColor ,profileBGColor,profileColor,subHeadingColor } from "../../Constants/Colors";
import { Bold, Medium, Regular } from "../../Constants/Fonts";


const styles = StyleSheet.create({
    container:{
        // marginTop:getStatusBarHeight() + 30,
        flex:1,
        backgroundColor:'white' 

    },
    header:{
        color:HeadingColor,
        fontFamily:Bold,
        fontSize:largeText,
        marginTop:Platform.OS=='android'?getStatusBarHeight(): getStatusBarHeight() + 30,


    },
    subHeading:{
        color:subHeadingColor ,
        fontSize:mediumText,
        fontFamily:Regular,
        marginTop:RFValue(5)

    },
    profileView:{
        marginVertical:20,
        justifyContent:'center',
        alignItems:'center'
    },
    profileOuterView:{
        width:100,
        height:100,
        borderRadius:100/2,
        backgroundColor:profileBGColor,
        justifyContent:'center',
        overflow:'hidden',
        alignItems:'center'
    },
    imgStyle:{
        width:100,
        height:100,
        backgroundColor:profileBGColor,
    },
    contentView:{
        marginHorizontal:35
    },
    inputContainerStyle:{
        // marginBottom:20,
        marginTop:20,
    },
    account:{
        marginTop:20,
        alignItems:'center',
        justifyContent:'center',
        flexDirection:'row'
    },
    leftText:{
        color:grey,
        fontFamily:Regular,
        fontSize:regularText
    },
    loginBtn:{
        marginLeft:3,
        fontSize:regularText,
        fontFamily:Bold,
        color:HeadingColor,
    },
    ORText:{
       marginTop:20,
       textAlign:'center',
       fontSize:mediumText,
       fontFamily:Bold
    },
    otherMethod:{
        color:profileColor,
        fontFamily:Regular,
        fontSize:smallText,
        textAlign:'center',
        marginTop:5,

    },
    socialLoginView:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
    },
    btnBG:{
        width:45,
        height:45,
        borderRadius:45/2,
        backgroundColor:'white',
        elevation:2,
        shadowColor:'#000',
        shadowOffset:{height:2,width:2},
        shadowOpacity:0.1,
        shadowRadius:2,
        marginTop:15,
        marginHorizontal:10,
        justifyContent:'center',
        alignItems:'center'
        
    },
    iconStyle:{
        color:HeadingColor,
        fontSize:25
    },
    errorText:{
        color:'red',
        fontFamily:Regular,
        fontSize:regularText,
        fontWeight:'500',
        marginTop:5,
    }
   


})
export default styles