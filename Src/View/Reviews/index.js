import React, { useEffect, useReducer, useRef } from 'react'
import { View, Text, Image, Pressable, FlatList, TouchableOpacity, ActivityIndicator } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Icon } from 'react-native-elements'

import { download } from '../../Constants/ConstantValues'
import AppButton from '../../Component/AppButton'
import styles from './styles';
import LGCircle from '../../Component/LGCircle'
import { HeadingColor } from '../../Constants/Colors'
import ReviewUser from '../../Component/ReviewAboutUser'
import ReviewWork from '../../Component/ReviewAboutWork'
import { USER } from '../../Constants/ConstantValues'
import { createReview, showCv } from '../../Utility/CvApi'
import AsyncStorage from "@react-native-async-storage/async-storage";
import { URL } from '../../Utility/server'
import { ferozi } from '../../Constants/Colors'
import ReviewsList from '../../Component/ReviewList'
import DialogBox from 'react-native-dialogbox';



function Reviews({ navigation, route }) {
    // console.log('route',route?.params)
    let dialogboxRef = useRef()
    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            isVisible: false,
            isWorkModalVisible: false,
            starCount: 1,
            attendace: 1,
            jobKnowledge: 1,
            attitude: 1,
            judgement: 1,
            reliability: 1,
            workfeedback: '',
            feedback: '',
            user: '',
            inProcess: false,
            loading: false,
            cv: {},
            reviews: '',
            showmenu: false,
            feedbackError:'',

        }
    )
    const {
        isVisible,
        starCount,
        feedback,
        isWorkModalVisible,
        attitude,
        attendace,
        jobKnowledge,
        reliability,
        judgement,
        workfeedback,
        user,
        inProcess,
        feedbackError,
        loading,
        cv,
        reviews,
        showmenu,
    } = state

    useEffect(() => {
        getCV()
        const unsubscribe = navigation.addListener('focus', () => {
            getUserInfo()
        });
        return unsubscribe;
    }, []);

    async function getUserInfo() {
        await AsyncStorage.getItem(USER).then((user) => {
            let userData = JSON.parse(user)
            updateState({ user: userData })
        })
    }
    function getCV() {
        updateState({ loading: true })
        let id = route.params?.id
        let obj = {
            id: id
        }
        showCv(obj).then((response) => {
            if (response.api_status) {
                updateState({ loading: false })
                updateState({ cv: response.cv, reviews: response?.reviews })
            } else {
                updateState({ loading: false })
            }
        }).catch((error) => {
            updateState({ loading: false })
            console.log('error is ', error)
        })
    }

    function renderStart(input) {
        const star = []
        for (let i = 0; i < input; i++) {
            star.push(
                <Icon
                    name={'star'}
                    type={'font-awesome'}
                    iconStyle={{ color: HeadingColor, paddingHorizontal: 2, fontSize: 20 }}
                />
            )
        }


        return (
            <View style={{ alignSelf: 'flex-start', flexDirection: 'row' }}>{star}</View>
        )
    }

    function handleOverlay() {
        updateState({ isVisible: !isVisible })
    }
    function onStarRatingPress(count) {
        console.log('onStarRating', count)
        updateState({ starCount: count })
    }
    function handleChangeText(text) {
        updateState({ feedback: text })
    }


    const handleWorkOverlay = () => {
        if (user) {
            updateState({ isWorkModalVisible: !isWorkModalVisible })
        } else {
            navigation.navigate('Auth')

        }
    }


    function typeYourReview(value) {
        updateState({ workfeedback: value , feedbackError:'' })

    }


    function onAttendancePress(value) {
        updateState({ attendace: value })
    }
    function onAttitudePress(value) {
        updateState({ attitude: value })
    }
    function onJudgementPress(value) {
        updateState({ judgement: value })
    }
    function onJobKnowledgePress(value) {
        updateState({ jobKnowledge: value })
    }
    function onReliabilityPress(value) {
        updateState({ reliability: value })
    }
    function submitWorkFeedback() {
        let userId = route?.params?.user_id
        let cv_id = route?.params?.id
        let error = false
        if (workfeedback != '' || workfeedback != undefined || workfeedback?.length == 0) {
            error = true
            updateState({feedbackError:'Please type your feedback'})
        }
        if (!error) {

            updateState({ inProcess: true })
            let obj = {
                content: workfeedback,
                cv_id: cv_id,
                user_id: userId
                // user_id: user?.id
            }
            createReview(obj).then((response) => {
                if (response?.api_status) {
                    updateState({ inProcess: false })
                    alert('thank you')
                    setTimeout(() => {
                        handleWorkOverlay()
                    }, 1000);
                } else {
                    console.log('some thing went wrong')
                }
            }).catch((error) => {
                console.log('error', error)
            })
        }
    }

    function renderHeader() {
        return (
            <View>
                <View style={{ ...styles.header }}>
                    <View style={styles.headerView}>
                        <View style={styles.userName}>
                            <View style={styles.imgBg}>
                                <Image
                                    source={userImgURI ? { uri: `${URL}${userImgURI}` } : download}
                                    style={styles.imgStyle}
                                />
                            </View>
                            <Text style={styles.nameStyle}>{cv?.user?.first_name}</Text>
                        </View>
                        {/* <AppButton
                            title={'View CV'}
                            btnText={styles.cvView}
                            gradientBtnStyle={styles.gradientBtnStyle}
                        /> */}
                    </View>

                </View>

                <View style={{ ...styles.contentView }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        {renderStart(5)}
                        <Text style={styles.startCount}>(5)</Text>
                    </View>
                    <Text style={styles.reviewText}>{'(5 Reviews)'}</Text>
                </View>
            </View>
        )
    }

    function handleReportComment() {
    }

    function openMenu() {
        if (user) {
            dialogboxRef.current.confirm({
                title: 'Message',
                content: ['Do you want to Report this Comments',],
                ok: {
                    text: 'Yes',
                    style: { color: 'red' },
                    callback: () => {
                        setTimeout(() => {
                            alert('Thank your for your feedback. we will get back to you soon');

                        }, 2000)
                    },
                },
                cancel: {
                    text: 'N0',
                    style: { color: 'blue' },
                    callback: () => {
                    },
                },
            })
        } else {
            navigation.navigate('Auth')
        }
        // updateState({ showmenu: !showmenu })

    }


    function renderReviews({ item, index }) {
        return (
            <View style={{ marginHorizontal: 20, marginTop: 20, }}>
                <ReviewsList
                    item={item}
                    openMenu={openMenu}
                // isVisible={showmenu}
                />
            </View>
        )
    }

    let userImgURI = cv?.user?.avatar?.url

    return (
        <View style={styles.container}>
            <View style={{ ...styles.header }}>
                <Pressable
                    onPress={() => navigation.goBack()}>
                    <Text style={styles.backText}>Back</Text>
                </Pressable>
            </View>

            {loading ?
                <View style={styles.inmiddlePage}>
                    <ActivityIndicator
                        color={ferozi}
                        size={'small'}
                    />
                </View> : reviews?.length > 0 ?
                    <FlatList
                        data={reviews}
                        renderItem={renderReviews}
                        keyExtractor={(item) => { item.id }}
                        ListHeaderComponent={renderHeader}
                    /> :
                    <View style={styles.inmiddlePage}>
                        <Text style={styles.normalText}>No Review Found</Text>
                    </View>
            }

            <TouchableOpacity
                style={styles.floaterBtn}>
                <LGCircle
                    iconName={'plus'}
                    iconType={'antdesign'}
                    iconStyle={{ color: 'white' }}
                    onPress={handleWorkOverlay}
                    gradientBtnStyle={{ width: 35, height: 35, borderRadius: 35 / 2 }}
                />
            </TouchableOpacity>

            <ReviewUser
                isVisible={isVisible}
                starCount={starCount}
                onChangeText={handleChangeText}
                feedback={feedback}
                onStarRatingPress={onStarRatingPress}
                onSubmitPress={handleOverlay}
            />
            <ReviewWork
                isVisible={isWorkModalVisible}
                attendace={attendace}
                onClose={handleWorkOverlay}
                onAttendancePress={onAttendancePress}
                attitude={attitude}
                onAttitudePress={onAttitudePress}
                jobKnowledge={jobKnowledge}
                onJobKnowledgePress={onJobKnowledgePress}
                judgement={judgement}
                onJudgementPress={onJudgementPress}
                reliability={reliability}
                onReliabilityPress={onReliabilityPress}
                workFeedback={workfeedback}
                inProcess={inProcess}
                feedbackError={feedbackError}
                typeYourReview={typeYourReview}
                handleWorkFeedback={handleWorkOverlay}
                submitWorkFeedback={submitWorkFeedback}
            />
            <DialogBox ref={dialogboxRef} />
        </View>
    )
}

export default Reviews
