import React from 'react'
import { Platform } from 'react-native'
import {View, Text,TextInput,StyleSheet,Image} from 'react-native'
import {Icon} from 'react-native-elements'
import { RFValue } from 'react-native-responsive-fontsize'
import { regularText } from '../Constants/AppStyle'
import { grey, HeadingColor, profileBGColor, profileColor } from '../Constants/Colors'
import { Regular } from '../Constants/Fonts'


const InputField=(props)=>{
const {iconName , iconType ,passIconName , passIconType,hideShowPassword, image} = props
    return(
        <View style={[styles.wraper,props.inputContainerStyle]}>
            {iconName ?
            <Icon
             name={iconName}
             type={iconType}
             iconStyle={[styles.icon,props.iconStyle]}
            />:image ?
            <Image
             source={image}
             style={{width:18,height:18}}
            />: null}
            <TextInput
                    ref={(c) => { textInput = c }}
                    underlineColorAndroid="transparent"
                    secureTextEntry={props.secureTextEntry}
                    editable={props.editable}
                    keyboardType={props.keyboardType}
                    placeholder={props.placeholder}
                    placeholderTextColor={props.placeholderTextColor|| grey}
                    onChangeText={props.onChangeText}
                    value={props.value}
                    multiline={props.multiline}
                    numberOfLines={props.numberOfLines}
                    textAlignVertical={props.textAlignVertical}
                    style={[styles.Input,props.InputStyle]}
                />
                <Icon
                name={passIconName}
                type={passIconType}
                iconStyle={styles.passIcon}
                onPress={hideShowPassword}
            />
        </View>
    )
}
export default InputField

const styles = StyleSheet.create({
wraper:{
    flexDirection:'row',
    alignItems:'center',
    borderRadius:20,
    borderColor:profileColor,
    borderWidth:1,
    // paddingVertical:10,
    paddingHorizontal:15,
},
icon:{
    fontSize:20,
    color:HeadingColor,
},
passIcon:{
    fontSize:20,
    color:grey
},
Input:{
    flex:1,
    fontFamily:Regular,
    fontSize:regularText,
    // backgroundColor:'red',
    marginLeft:10,
    paddingVertical:Platform.OS=='android'? 8:12,
}
})