import React, { useEffect, useReducer } from 'react'
import { View, Text, TouchableOpacity, Image, KeyboardAvoidingView } from 'react-native'
import SIcon from 'react-native-vector-icons/SimpleLineIcons';
import { launchImageLibrary } from "react-native-image-picker"
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Icon } from 'react-native-elements'

import { ferozi, HeadingColor, profileColor, subHeadingColor } from '../../Constants/Colors';
import { options } from '../../Constants/ConstantValues';
import InputField from '../../Component/InputField';
import AppButton from '../../Component/AppButton'
import LGCircle from '../../Component/LGCircle'
import { pencil, file } from '../../Constants/ConstantValues'
import styles from './styles';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import BottomTabs from '../../Component/BottomTabs';
import { getBottomSpace } from 'react-native-iphone-x-helper';



function Profile({ navigation }) {

  useEffect(() => {
  }, [])

  const [state, updateState] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
      filePath: '',
      name: '', email: '', phone: '',

    }
  )
  const {
    filePath,
    name,
    email,
    phone,

  } = state


  function opneImagePicker() {

    launchImageLibrary(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };
        updateState({
          filePath: response.assets[0],
          //  fileData: response.data,
          //  fileUri: response.uri
        });
      }
    });
  }

  function saveChanges() { }


  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView>

        <View style={{ marginHorizontal: 20 }}>
          <Text style={styles.header}>My Profile</Text>
        </View>

        <View style={styles.profileView}>
          <TouchableOpacity
            style={styles.profileOuterView}
          // onPress={opneImagePicker}
          >
            {!filePath ?
              <SIcon name="user" style={{ fontSize: 40, color: profileColor }} />
              :
              <View style={styles.imgStyle}>
                <Image source={{ uri: filePath.uri }} style={{ width: null, height: null, flex: 1 }} />
              </View>
            }
          </TouchableOpacity>
          <LGCircle
            onPress={opneImagePicker}
            gradientBtnStyle={styles.gradientBtnStyle}
            iconName={'pencil'}
            iconType={'octicon'}
            iconStyle={{ fontSize: 15, color: 'white' }}
          />
        </View>


        <View style={styles.contentView}>
          <InputField
            placeholder={'Name'}
            iconName={'user'}
            iconType={'font-awesome'}
            onChangeText={(text) => updateState({ name: text })}
            inputContainerStyle={styles.inputContainerStyle}
          />
          <InputField
            placeholder={'Email'}
            iconName={'email'}
            iconType={'material'}
            onChangeText={(text) => updateState({ email: text })}
            inputContainerStyle={styles.inputContainerStyle}
          />
          <InputField
            placeholder={'Phone Number'}
            iconName={'phone-alt'}
            iconType={'font-awesome-5'}
            onChangeText={(text) => updateState({ phone: text })}
            inputContainerStyle={styles.inputContainerStyle}
          />

          <View style={styles.cvView}>
            <View style={styles.cvInerView}>
              <LGCircle
                img={file}
                color1={ferozi}
                color2={HeadingColor}
                imgStyle={{ width: 30, height: 30, }}
                gradientBtnStyle={{ width: 50, height: 50, borderRadius: 50 / 2 }}
              />
              <Text style={styles.editCV}>Edit your CV</Text>
            </View>
            <Icon
              name={'chevron-forward'}
              type={'ionicon'}
              iconStyle={{ fontSize: 23 }}
            />
          </View>
        </View>
        <AppButton
          title={'SAVE CHANGES'}
          onPress={saveChanges}
          gradientBtnStyle={{ marginTop: 10, width: wp(70) }}
        />

      </KeyboardAwareScrollView>

      <View style={{ marginBottom:getBottomSpace() }}>
        <BottomTabs
          homePress={() => navigation.navigate('Detail')}
          chatPress={() => navigation.navigate('Chat')}
        />
      </View>

    </View>
  )
}
export default Profile