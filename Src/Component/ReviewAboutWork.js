import React from 'react'
import { View, Text, StyleSheet, Platform, ActivityIndicator } from 'react-native'
import { Overlay } from 'react-native-elements';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import StarRating from 'react-native-star-rating';


import LGCircle from './LGCircle';
import { review } from '../Constants/ConstantValues'
import { mediumText, regularText } from '../Constants/AppStyle';
import { grey, HeadingColor, lightYellow, profileBGColor, profileColor, ferozi } from '../Constants/Colors';
import { Medium, Regular } from '../Constants/Fonts';
import InputField from './InputField';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AppButton from './AppButton';


const imgMarginBottom = 40
const imgHeight = 80
const imgWidth = 80
const topMargin = 15
const horizontalPadding = 25

const RateReview = (props) => {
    const { isVisible,
        attendace,
        onClose,
        onAttendancePress,
        attitude,
        onAttitudePress,
        jobKnowledge,
        onJobKnowledgePress,
        judgement,
        onJudgementPress,
        reliability,
        onReliabilityPress,
        workFeedback,
        handleWorkFeedback,
        submitWorkFeedback,
        typeYourReview,
        inProcess,
        feedbackError,
    } = props

    // console.log('isVisible', props)

    return (
        <KeyboardAwareScrollView>
            <Overlay
                isVisible={isVisible}
                useNativeDriver={true}
                onBackdropPress={onClose}
                animationIn={props.animationIn}
                animationInTiming={props.animationInTiming}
                animationOut={props.animationOut}
                overlayStyle={styles.overlayStyle}
            // style={{marginBottom:0}}
            >
                <View style={styles.modalStyling}>
                    <View style={styles.imgContainer}>
                        <LGCircle
                            startX={0.7}
                            img={review}
                            imgStyle={{ width: 40, height: 40 }}
                            //  iconName={'plus'}
                            //  iconType={'antdesign'}
                            //  iconStyle={{color:'white'}}
                            gradientBtnStyle={{ width: imgWidth, height: imgHeight, borderRadius: imgHeight / 2 }}
                        />
                    </View>
                    <View style={{ marginTop: -imgMarginBottom + 15 }}>
                        <Text style={styles.header}>Please rate & give a review!</Text>
                        <View style={[styles.row, { marginTop: 20 }]}>
                            <Text style={styles.textStyle}>Attendance/Punctuality</Text>
                            <StarRating
                                disabled={false}
                                maxStars={5}
                                rating={attendace}
                                selectedStar={onAttendancePress}
                                fullStarColor={lightYellow}
                                emptyStarColor={profileColor}
                                emptyStar={'star'}
                                fullStar={'star'}
                                containerStyle={{ width: '30%', marginVertical: 10, alignSelf: 'center' }}
                                starSize={15}
                            />

                        </View>

                        <View style={styles.row}>
                            <Text style={styles.textStyle}>Attitude</Text>
                            <StarRating
                                disabled={false}
                                maxStars={5}
                                rating={attitude}
                                selectedStar={onAttitudePress}
                                fullStarColor={lightYellow}
                                emptyStarColor={profileColor}
                                emptyStar={'star'}
                                fullStar={'star'}
                                containerStyle={{ width: '30%', marginVertical: 10, alignSelf: 'center' }}
                                starSize={15}
                            />
                        </View>

                        <View style={styles.row}>
                            <Text style={styles.textStyle}>Job Knowledge</Text>
                            <StarRating
                                disabled={false}
                                maxStars={5}
                                rating={jobKnowledge}
                                selectedStar={onJobKnowledgePress}
                                fullStarColor={lightYellow}
                                emptyStarColor={profileColor}
                                emptyStar={'star'}
                                fullStar={'star'}
                                containerStyle={{ width: '30%', marginVertical: 10, alignSelf: 'center' }}
                                starSize={15}
                            />
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.textStyle}>Judgement/Decision making </Text>
                            <StarRating
                                disabled={false}
                                maxStars={5}
                                rating={judgement}
                                selectedStar={onJudgementPress}
                                fullStarColor={lightYellow}
                                emptyStarColor={profileColor}
                                emptyStar={'star'}
                                fullStar={'star'}
                                containerStyle={{ width: '30%', marginVertical: 10, alignSelf: 'center' }}
                                starSize={15}
                            />
                        </View>

                        <View style={styles.row}>
                            <Text style={styles.textStyle}>Reliability/Dependability</Text>
                            <StarRating
                                disabled={false}
                                maxStars={5}
                                rating={reliability}
                                selectedStar={onReliabilityPress}
                                fullStarColor={lightYellow}
                                emptyStarColor={profileColor}
                                emptyStar={'star'}
                                fullStar={'star'}
                                containerStyle={{ width: '30%', marginVertical: 10, alignSelf: 'center' }}
                                starSize={15}
                            />
                        </View>


                        <InputField
                            placeholder={'Share Your reviews'}
                            onChangeText={typeYourReview}
                            value={workFeedback}
                            InputStyle={styles.textArea}
                            iconStyle={{ marginTop: 3, }}
                            inputContainerStyle={{ ...styles.inputContainerStyle, alignItems: 'flex-start', paddingVertical: Platform.OS == 'ios' ? 5 : 0, }}
                            multiline={true}

                        />
                        {feedbackError?
                        <Text style={styles.errorText} >{feedbackError}</Text>: null}
                        {inProcess ?
                            <View style={{ marginTop: 10 }}>
                                <ActivityIndicator
                                    color={ferozi}
                                    size={'small'}
                                />
                            </View> :
                            <AppButton
                                title={'Submit'}
                                onPress={submitWorkFeedback}
                                gradientBtnStyle={{ marginTop: 10, width: wp(30), }}
                            />
                        }
                    </View>

                </View>

            </Overlay>
        </KeyboardAwareScrollView>
    )
}

export default RateReview;

const styles = StyleSheet.create({
    overlayStyle: {
        backgroundColor: 'transparent',
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        borderBottomRightRadius: 15,
        borderBottomLeftRadius: 15,
        width: wp(90),
        paddingLeft: 0,
        paddingBottom: 0,
        paddingTop: 0,
        paddingRight: 0
    },
    modalStyling: {
        backgroundColor: 'white',
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        borderBottomRightRadius: 15,
        borderBottomLeftRadius: 15,
        paddingBottom: 20,
        paddingHorizontal: 20
        // marginHorizontal: 25,
    },

    imgContainer: {
        width: imgWidth,
        height: imgHeight,
        borderRadius: imgHeight / 2,
        alignItems: 'center',
        alignSelf: 'center',
        bottom: imgMarginBottom,
    },
    contentStyling: {
        paddingHorizontal: horizontalPadding,
        marginTop: topMargin / 2,
        marginBottom: topMargin,
        justifyContent: 'center',
        alignItems: 'center',
    },
    header: {
        textAlign: 'center',
        fontFamily: Medium,
        fontSize: mediumText,
        color: HeadingColor
    },
    textArea: {
        height: 80,
        justifyContent: "flex-start",
        textAlignVertical: 'top'
    },
    inputContainerStyle: {
        // marginBottom: 20,
        backgroundColor: profileBGColor,
        paddingHorizontal: 0,
        borderWidth: 0,
        borderRadius: 10,
        marginTop: 20,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 0,
        marginTop: -2,
    },
    textStyle: {
        color: grey,
        fontFamily: Regular,
        fontSize: regularText
    },
    errorText:{
        color:'red',
        fontFamily:Regular,
        fontSize:regularText,
        fontWeight:'500',
        marginTop:5,
    }

})