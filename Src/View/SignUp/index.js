import React, { useEffect, useReducer } from 'react'
import { View, Text, TouchableOpacity, Image, KeyboardAvoidingView, ActivityIndicator } from 'react-native'
import SIcon from 'react-native-vector-icons/SimpleLineIcons';
import { launchImageLibrary } from "react-native-image-picker"
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Icon } from 'react-native-elements'

import { HeadingColor, profileColor, ferozi } from '../../Constants/Colors';
import { options } from '../../Constants/ConstantValues';
import InputField from '../../Component/InputField';
import AppButton from '../../Component/AppButton'
import styles from './styles';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { ValidateEmail } from '../../Utility/RandomFun';
import { Register } from '../../Utility/AuthAPi';



function SignUp({ navigation }) {

  useEffect(() => {
  }, [])

  const [state, updateState] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
      filePath: '',
      securePass: true,
      secureConfirmPass: true,
      name: '', email: '', phone: '',
      password: '', confirmPass: '',
      nameError: '', passwordError: '', emailError: '',
      confirmPassError: '', phoneError: '',
      inProcess: false,
    }
  )
  const {
    filePath,
    secureConfirmPass,
    securePass,
    name,
    email,
    phone,
    password,
    confirmPass,

    nameError,
    passwordError,
    confirmPassError,
    emailError,
    phoneError,
    inProcess,
  } = state


  function opneImagePicker() {

    launchImageLibrary(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };
        updateState({
          filePath: response.assets[0],
          //  fileData: response.data,
          //  fileUri: response.uri
        });
      }
    });
  }

  function hideShowPassword() {
    updateState({ securePass: !securePass })
  }
  function hideShowConfirmPassword() {
    updateState({ secureConfirmPass: !secureConfirmPass })
  }



  function handleSignUp() {
    let error = false
    if (!name || name?.length == 0 || name == '') {
      updateState({ nameError: 'Please enter name' })
    }
    if (!ValidateEmail(email)) {
      updateState({ emailError: 'Valid email required' })
    }
    if (!password || password?.length == 0 || password == '') {
      updateState({ passwordError: 'Please enter password' })
    }
    if (!confirmPass || confirmPass?.length == 0 || confirmPass == '') {
      updateState({ confirmPassError: 'Password not matched' })
    }
    if (password != confirmPass) {
      updateState({ confirmPassError: 'Password not matched' })
    }
    if (!phone || phone?.length == 0 || phone == '') {
      updateState({ phoneError: 'Password enter phone' })

    }

    if (!error) {
      updateState({ inProcess: true })
      let obj = {
        first_name: name,
        email: email,
        password: password,
        contact_number: phone,
        locale: 'en'
      }
      Register(obj).then((response) => {
        updateState({ inProcess: false })
        if(response?.user){
           navigation.navigate('Login')
           updateState({email:'',name:'', password:'' , phone:'' , confirmPass:''})
        }else{
          console.log('eror Register a user', error)
        }

      }).catch((error) => {
        updateState({ inProcess: false })
        console.log('error while Register a user', error)
      })


    }

  }
  function handleLogin() { navigation.navigate('Login') }

  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView>

        <View style={{ marginHorizontal: 20 }}>
          <Text style={styles.header}>Sign Up</Text>
          <Text style={styles.subHeading}>Sign up to create your account</Text>
        </View>

        <View style={styles.profileView}>
          <TouchableOpacity
            style={styles.profileOuterView}
            onPress={opneImagePicker}
          >
            {!filePath ?
              <SIcon name="user" style={{ fontSize: 40, color: profileColor }} />
              :
              <View style={styles.imgStyle}>
                <Image source={{ uri: filePath.uri }} style={{ width: null, height: null, flex: 1 }} />
              </View>
            }
          </TouchableOpacity>
        </View>

        <View style={styles.contentView}>
          <InputField
            placeholder={'Name'}
            iconName={'user'}
            iconType={'font-awesome'}
            onChangeText={(text) => updateState({ name: text, nameError: '' })}
            inputContainerStyle={styles.inputContainerStyle}
          />
          {nameError ?
            <Text style={styles.errorText}>{nameError}</Text> : null}
          <InputField
            placeholder={'Email'}
            iconName={'email'}
            iconType={'material'}
            onChangeText={(text) => updateState({ email: text, emailError: '' })}
            inputContainerStyle={styles.inputContainerStyle}
          />
          {nameError ?
            <Text style={styles.errorText}>{nameError}</Text> : null}


          <InputField
            placeholder={'Phone Number'}
            iconName={'phone-alt'}
            iconType={'font-awesome-5'}
            onChangeText={(text) => updateState({ phone: text, phoneError: '' })}
            inputContainerStyle={styles.inputContainerStyle}
          />
          {phoneError ?
            <Text style={styles.errorText}>{phoneError}</Text> : null}

          <InputField
            placeholder={'Password'}
            iconName={'locked'}
            iconType={'fontisto'}
            passIconType={'entypo'}
            onChangeText={(text) => updateState({ password: text, passwordError: '' })}
            secureTextEntry={securePass}
            hideShowPassword={hideShowPassword}
            passIconName={securePass ? 'eye-with-line' : 'eye'}
            inputContainerStyle={styles.inputContainerStyle}
          />

          {passwordError ?
            <Text style={styles.errorText}>{passwordError}</Text> : null}
          <InputField
            placeholder={'Confirm Password'}
            iconName={'locked'}
            iconType={'fontisto'}
            passIconType={'entypo'}
            onChangeText={(text) => updateState({ confirmPass: text, confirmPassError: '' })}
            secureTextEntry={secureConfirmPass}
            hideShowPassword={hideShowConfirmPassword}
            passIconName={secureConfirmPass ? 'eye-with-line' : 'eye'}
            inputContainerStyle={styles.inputContainerStyle}
          />
          {confirmPassError ?
            <Text style={styles.errorText}>{confirmPassError}</Text> : null}
        </View>
        {inProcess ?
        <View style={{marginTop:20}}>
          <ActivityIndicator
            color={ferozi}
            size={'small'}
          />
          </View>
          :
          <AppButton
            title={'SIGNUP'}
            onPress={handleSignUp}
            gradientBtnStyle={{ marginTop: 30, width: wp(70) }}
          />
        }
        <TouchableOpacity onPress={handleLogin}>
          <View style={styles.account}>
            <Text style={styles.leftText}>Already have an account?</Text>
            <Text style={styles.loginBtn}>Log in</Text>
          </View>
        </TouchableOpacity>

        {/* <Text style={styles.ORText}>OR</Text>
        <Text style={styles.otherMethod}>Sing in using</Text> */}

        {/* <View style={styles.socialLoginView}>
          <View style={styles.btnBG}>
            <Icon name={'google'} type={'fontisto'} iconStyle={styles.iconStyle} />
          </View>
          <View style={styles.btnBG}>
            <Icon name={'facebook'} type={'fontisto'} iconStyle={styles.iconStyle} />
          </View>
          <View style={styles.btnBG}>
            <Icon name={'apple'} type={'fontisto'} iconStyle={styles.iconStyle} />
          </View>
        </View> */}
      </KeyboardAwareScrollView>

    </View>
  )
}
export default SignUp