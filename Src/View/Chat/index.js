import React, { useEffect, useReducer, useRef, } from 'react'
import {
  View, Text, TouchableOpacity,
  SafeAreaView, TextInput,
  FlatList, Platform,
  SectionList,
  Keyboard
} from 'react-native'
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { getBottomSpace } from 'react-native-iphone-x-helper'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Icon, Divider, Slider } from 'react-native-elements'
import RNFetchBlob from 'rn-fetch-blob'
import AudioRecorderPlayer, {
  AVEncoderAudioQualityIOSType,
  AVEncodingOption,
  AudioEncoderAndroidType,
  AudioSet,
  AudioSourceAndroidType,
} from 'react-native-audio-recorder-player';
import { PermissionsAndroid } from 'react-native-permissions';
import EmojiSelector, { Categories } from "react-native-emoji-selector";
import useKeyboardHeight from 'react-native-use-keyboard-height';
import { launchImageLibrary,launchCamera , } from "react-native-image-picker"
import DocumentPicker, {types , allowMultiSelection} from 'react-native-document-picker'


import styles from './styles'
import { black, grey, HeadingColor, profileBGColor, profileColor } from '../../Constants/Colors';
import { options } from '../../Constants/ConstantValues';

const audioRecorderPlayer = new AudioRecorderPlayer();
audioRecorderPlayer.setSubscriptionDuration(0.1)

let data = [
  { id: 1, type: 'send', name: 'User Reply will goes here User Reply will goes here User Reply will goes hereUser Reply will goes here User Reply will goes here User Reply will goes here' },
  { id: 2, type: 'send', name: 'User Reply will goes here User Reply will goes here User Reply will goes hereUser Reply will goes here User Reply will goes here User Reply will goes here' },
  { id: 3, type: 'recv', name: 'User Reply will goes here User Reply will goes here User Reply will goes hereUser Reply will goes here User Reply will goes here User Reply will goes here' },
  { id: 4, type: 'send', name: 'zah' },
  { id: 5, type: 'send', name: 'User Reply will goes here User Reply will goes here User Reply will goes hereUser Reply will goes here User Reply will goes here User Reply will goes here' },
  { id: 6, type: 'recv', name: 'zaheer' },
  { id: 7, type: 'recv', name: 'zaheer' },
  { id: 8, type: 'send', name: 'User Reply will goes here User Reply will goes here User Reply will goes hereUser Reply will goes here User Reply will goes here User Reply will goes here' },
  { id: 9, type: 'send', name: 'User Reply will goes here User Reply will goes here User Reply will goes hereUser Reply will goes here User Reply will goes here User Reply will goes here' },
  { id: 10, type: 'recv', name: 'majeed' },
  { id: 11, type: 'send', name: 'zafar' },
]


function Chat({ navigation }) {
  let flatListRef = useRef()
  let inputRef = useRef()
  const keyboardHeight = useKeyboardHeight();

  const [state, updateState] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
      message: '',
      startRec: false,
      recordSecs: 0,
      recordTime: '00:00:00',
      currentPositionSec: 0,
      currentDurationSec: 0,
      playTime: '00:00:00',
      duration: '00:00:00',
      filePath: '',
      keyboardOffset: undefined,
      emoji_Visible: false,
      stopRec: false,
      seekMaxValue:10,
      seekValue:0,
      isRecPlaying:false,
      camDoc:undefined
    }
  )
  const { message,
    startRec,
    stopRec,
    filePath,
    keyboardOffset,
    emoji_Visible,
    recordTime,
    playTime,
    duration,
    currentPositionSec,
    seekMaxValue,
    seekValue,
    isRecPlaying,
    camDoc,
  } = state

  useEffect(() => {

    requesPermissions()
    Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
    Keyboard.addListener("keyboardDidHide", _keyboardDidHide);

    return () => {
      Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
      Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
      audioRecorderPlayer.removePlayBackListener((e) => {});
    };

  }, [])

  const _keyboardDidShow = (event) => {
    console.log("Keyboard Shown", typeof (event.endCoordinates.height));
    updateState({ emoji_Visible: false, keyboardOffset: event.endCoordinates.height })
  };

  const _keyboardDidHide = () => {
    // console.log("Keyboard Hidden");
    // updateState({ keyboardOffset: 0 })
  };


  const setFocus = () => {
    updateState({ message: '' })
    inputRef.current.focus()
  }

  const renderItem = ({ item, index }) => {
    return (
      <View>
        {item.type == 'send' ?
          <View style={styles.sendMsg}>
            <Text style={styles.sendMsgText}>{item.name}</Text>
            <Text style={{ textAlign: 'right', color: 'white' }}>{'12:45 pm'}</Text>
          </View>
          : item.type == 'recv' ?
            <View style={styles.recvMsg}>
              <Text style={styles.recvMsgText}>{item.name}</Text>
              <Text style={{ textAlign: 'right' }}>{'12:45 pm'}</Text>
            </View> : null}
      </View>
    )
  }

  const requesPermissions = async () => {
    if (Platform.OS === 'android') {
      try {
        const grants = await PermissionsAndroid.requestMultiple([
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
          PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
        ]);

        console.log('write external stroage', grants);

        if (
          grants['android.permission.WRITE_EXTERNAL_STORAGE'] ===
          PermissionsAndroid.RESULTS.GRANTED &&
          grants['android.permission.READ_EXTERNAL_STORAGE'] ===
          PermissionsAndroid.RESULTS.GRANTED &&
          grants['android.permission.RECORD_AUDIO'] ===
          PermissionsAndroid.RESULTS.GRANTED
        ) {
          console.log('Permissions granted');
        } else {
          console.log('All required permissions not granted');
          return;
        }
      } catch (err) {
        console.warn(err);
        return;
      }
    }

  }

  const onStartRecord = async () => {

    const dirs = RNFetchBlob.fs.dirs;
    const path = Platform.select({
      ios: 'hello.m4a',
      android: `${dirs.CacheDir}/hello.mp3`,
    });

    const audioSet = {
      AudioEncoderAndroid: AudioEncoderAndroidType.AAC,
      AudioSourceAndroid: AudioSourceAndroidType.MIC,
      AVEncoderAudioQualityKeyIOS: AVEncoderAudioQualityIOSType.high,
      AVNumberOfChannelsKeyIOS: 2,
      AVFormatIDKeyIOS: AVEncodingOption.aac,
    };
    // console.log('audioSet', audioSet);
    updateState({ 
      startRec: true,
      stopRec: false,
      filePath: path ,
      seekValue:0,
      isRecPlaying:false
     })
    const result = await audioRecorderPlayer.startRecorder(path, audioSet);
    audioRecorderPlayer.addRecordBackListener((e) => {
      console.log('addRecordBackListner', e)
      let _recTime = audioRecorderPlayer.mmssss( Math.floor(e.currentPosition))
      let [rm,rs,rss]  = _recTime.split(':')
      let _seekValue =  (Number(rm) * 60 ) + Number(rs)
      updateState({
        recordSecs: e.currentPosition,
        recordTime:`${rm}:${rs}`,
        seekMaxValue : _seekValue,
      });

    });
    console.log('recording result', result);
  };

  const onStopRecord = async () => {
    console.log('onStop',)
    audioRecorderPlayer.removeRecordBackListener((e) => {
      console.log('remove record back listner', e)
    });
    const result = await audioRecorderPlayer.stopRecorder();
    console.log('stop recorder:', result);
    updateState({ stopRec: true })

  }

  const onStartPlay = async () => {
    console.log('onStartPlay', filePath);

    if (filePath) {
      const msg = await audioRecorderPlayer.startPlayer(filePath);
      audioRecorderPlayer.setVolume(1.0);
      updateState({isRecPlaying:true})
      // console.log('msg' ,msg);
      audioRecorderPlayer.addPlayBackListener((e) => {
        if (e.currentPosition === e.duration) {
          console.log('finished');
          audioRecorderPlayer.stopPlayer();
        }

       let _playTime = audioRecorderPlayer.mmssss( Math.floor(e.currentPosition))
       let [pm,ps,pss]  = _playTime.split(':')
        let _Value =  (Number(pm) * 60 ) + Number(ps)

       let _duration = audioRecorderPlayer.mmssss(Math.floor(e.duration)) 
       let [dm , ds , dss] = _duration.split(':')
        updateState({
          currentPositionSec: e.currentPosition,
          currentDurationSec: e.duration,
          playTime:`${pm}:${ps}`,
          duration: `${dm}:${ds}`,
          seekValue:_Value,
        });
      });
    } else {
      console.log('file not available')
    }
  }

const onPausePlay = async()=>{
  await audioRecorderPlayer.pausePlayer();
  updateState({isRecPlaying:false})
}


  const deleteRecording = async () => {
    const result = await audioRecorderPlayer.stopRecorder();
    console.log('stop recorder in delete:', result);

    audioRecorderPlayer.removeRecordBackListener((e) => {
      console.log('remove record back listner in delete', e)
    });
    updateState({ startRec: false })
  }

  const showEmojiKeyboard = () => {
    if (!emoji_Visible) {
      Keyboard.dismiss()
      setTimeout(() => {
        updateState({ emoji_Visible: true })
      }, 0)
    } else {
      inputRef.current.focus()
      updateState({ emoji_Visible: false })
    }
  }

  const openCamera =()=>{
    launchCamera(options, (response) => {
    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    } else {
      console.log('response',response)
      const source = { uri: response.uri };
      updateState({
        camDoc: response.assets[0],
        //  fileData: response.data,
        //  fileUri: response.uri
      });
    }
  });

  }

  const openAttechments =async()=>{
    const res = await DocumentPicker.pick({
      type: [DocumentPicker.types.allFiles],
      allowMultiSelection:false
    });

    console.log('here is result', res)
  }

  return (
    <View style={styles.container}>
      <View style={{ marginHorizontal: 20 }}>
        <Text style={styles.header}>My Profile</Text>
      </View>
      <FlatList
        contentContainerStyle={styles.flatListStyle}
        data={data}
        keyExtractor={(item) => item.id}
        renderItem={renderItem}
        ref={flatListRef}
        scrollEnabled={true}
        keyboardDismissMode="on-drag"
        keyboardShouldPersistTaps='handled'
        bounces={false}
        onContentSizeChange={() => flatListRef.current.scrollToEnd({ animated: true })}
        onLayout={() => flatListRef.current.scrollToEnd({ animated: true })}
        listKey={(item, index) => { 'A' + item }}
      />

      {/* <SectionList
        style={styles.flatListStyle}
        ref={flatListRef}
        sections={data}
        keyExtractor={(item, index) => item + index}
        renderItem={renderItem}
        renderSectionHeader={({ section: { title } }) => (<Text style={styles.title}>{title}</Text>)}
        showsVerticalScrollIndicator={false}
        stickySectionHeadersEnabled={false}  
      /> */}



      {startRec ?
        <View style={styles.outerView}>
          <View style={styles.voiceRecorderView}>
            <View style={styles.voiceRecorderInnerView}>
              <View style={styles.timerView}>

                {stopRec ?
                  <View style={[styles.playRecordView]}>
                    <Icon
                     onPress={isRecPlaying?onPausePlay :onStartPlay}
                      name={isRecPlaying?'pause':'play-arrow'}
                      type={'material'}
                      iconStyle={{ fontSize: 40, color: grey }}
                    />
                    <Slider
                      value={seekValue}
                      // onValueChange={setValue}
                      maximumValue={seekMaxValue}
                      minimumValue={0}
                      style={{ width: '70%', }}
                      step={1}
                      allowTouchTrack
                      maximumTrackTintColor={'#b3b3b3'}
                      minimumTrackTintColor={HeadingColor}
                      trackStyle={{ height: 5, backgroundColor: 'transparent' }}
                      thumbStyle={{ height: 20, width: 20, backgroundColor: 'transparent' }}
                      thumbProps={{
                        children: (
                          <Icon
                            name="circle"
                            type="material-community"
                            iconStyle={{ fontSize: 20, color: HeadingColor }}
                            containerStyle={{ bottom: 0, right: 0, }}
                          />
                        ),
                      }}
                    />
                    <Text>{recordTime}</Text>
                  </View>
                  :
                  <Text>{recordTime}</Text>

                }

              </View>
              <View style={styles.actionView}>
                <Icon
                  onPress={deleteRecording}
                  name={'delete'} type={'material'}
                  iconStyle={styles.deleteIcon}
                  containerStyle={styles.deleteIconStyle}
                />
                {!stopRec &&
                <Icon
                  onPress={onStopRecord}
                  name={'stop-circle-outline'} type={'ionicon'}
                  iconStyle={styles.pauseIcon}
                  containerStyle={styles.deleteIconStyle}
                />
                }
                <Icon
                  name={'send'}
                  type={'material'}
                  iconStyle={styles.sendIconStyle}
                  containerStyle={{ alignSelf: 'flex-end' }}
                />
              </View>
            </View>
          </View>
        </View>
        :
        <View style={styles.outerView}>
          <View style={styles.innerView}>
            <Icon
              onPress={showEmojiKeyboard}
              name={emoji_Visible ? 'keyboard' : 'emoji-emotions'}
              type={'material'}
              containerStyle={styles.iconContainerStyle}
              iconStyle={styles.iconStyle}
            />
            <TextInput
              placeholder={"Type here"}
              placeholderTextColor={grey}
              onChangeText={(text) => {
                console.log('text', text)
                updateState({ message: text })
              }}
              value={message}
              multiline={true}
              blurOnSubmit={false}
              autoFocus={false}
              ref={inputRef}
              style={styles.inputStyle}
            />
            <Icon
              onPress={openAttechments}
              name={'attachment'}
              type={'entypo'}
              containerStyle={styles.iconContainerStyle}
              iconStyle={styles.attachIconStyle}
            />
            {message.length > 0 ? null :
            <Icon
              onPress={openCamera}
              name={'camera'}
              type={'fontisto'}
              containerStyle={styles.iconContainerStyle}
              iconStyle={styles.attachIconStyle}
            /> 
            }
          </View>


          {message.trim().length > 0 ?
            <TouchableOpacity
              style={{ alignSelf: 'flex-end', marginBottom: 5, marginLeft: 10, marginRight: 10 }}
              onPress={() => setFocus()}
            >
              <Icon
                name={'send'}
                type={'material'}
                iconStyle={styles.sendIconStyle}
                containerStyle={{ alignSelf: 'flex-end' }}
              />
            </TouchableOpacity> :
            <Icon
              onPress={startRec ? onStopRecord : onStartRecord}
              name={'microphone'}
              type={'font-awesome'}
              containerStyle={{ alignSelf: 'flex-end', marginBottom: 5, marginLeft:0, marginRight:5 }}
              iconStyle={{
                fontSize: 40,
                marginLeft: 10,
                color: startRec ? HeadingColor : black,
              }}
            />
          }
        </View>
      }





      {emoji_Visible &&
        <View style={{ width: '100%', height: keyboardOffset || 330 }}>
          <EmojiSelector
          // style={{width:20,}}
            onEmojiSelected={emoji => {
              let c = message
              c = c.concat(emoji)
              updateState({ message: c })
            }}
            showSearchBar={false}
            showTabs={true}
            showHistory={true}
            showSectionTitles={true}
            category={Categories.all}
          />
          
        </View>
      }



      {Platform.OS === 'ios' ?
        <KeyboardSpacer /> : null}
    </View>
  )
}
export default Chat

