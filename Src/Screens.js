import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';


import Splash from './View/Splash'
import Login from './View/Login'
import SignUp from './View/SignUp';
import ForgotPassword from './View/ForgotPassword'
import PasswordChange from './View/PasswordChange'
import PhoneVerification from './View/PhoneVerification'
import Verified from './View/Verified'
import Home from './View/Home';
import Profile from './View/Profile'
import Education from './View/Detail/Education';
import Experience from './View/Detail/Experience'
import Achievements from './View/Detail/Achievements'
import Skills from './View/Detail/Skills'
import Hobbies from './View/Detail/Hobbies'
import Reviews from './View/Reviews'
import Chat from './View/Chat'
import Sqlite from './View/Sqlite';


const Stack = createNativeStackNavigator();

function SplashNavigator() {
    return (

        <Stack.Navigator initialRouteName={"Splash"} screenOptions={{ headerShown: false }}>
            <Stack.Screen name={'Splash'} component={Splash} />
        </Stack.Navigator>
    )
}
function AuthNavigator() {
    return (
        <Stack.Navigator initialRouteName={"Login"} screenOptions={{ headerShown: false }}>
            <Stack.Screen name={'Sqlite'} component={Sqlite} />
            <Stack.Screen name={'Login'} component={Login} />
            <Stack.Screen name={'SignUp'} component={SignUp} />
            <Stack.Screen name={'ForgotPassword'} component={ForgotPassword} />
            <Stack.Screen name={'PasswordChange'} component={PasswordChange} />
            <Stack.Screen name={'PhoneVerification'} component={PhoneVerification} />
            <Stack.Screen name={'Verified'} component={Verified} />
        </Stack.Navigator>
    )
}


function DetailNavigator() {
    return (
        <Stack.Navigator initialRouteName={"Education"} screenOptions={{ headerShown: false }}>
            <Stack.Screen name={'Education'} component={Education} />
            <Stack.Screen name={'Experience'} component={Experience} />
            <Stack.Screen name={'Achievements'} component={Achievements} />
            <Stack.Screen name={'Skills'} component={Skills} />
            <Stack.Screen name={'Hobbies'} component={Hobbies} />
        </Stack.Navigator>
    )
}

function AppNavigator() {
    return (
        <Stack.Navigator initialRouteName={"Home"} screenOptions={{ headerShown: false }}>
            <Stack.Screen name={'Home'} component={Home} />
            <Stack.Screen name={'Profile'} component={Profile} />
            <Stack.Screen name={'Reviews'} component={Reviews}/>
            <Stack.Screen name={'Chat'} component={Chat}/>
        </Stack.Navigator>
    )
}

function AppContainer() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Splash" screenOptions={{ headerShown: false }}>
                <Stack.Screen name={'Splash'} component={SplashNavigator} />
                <Stack.Screen name={'Auth'} component={AuthNavigator} />
                <Stack.Screen name={'Detail'} component={DetailNavigator} />
                <Stack.Screen name={'App'} component={AppNavigator} />

            </Stack.Navigator>
        </NavigationContainer>
    )
}
export default AppContainer