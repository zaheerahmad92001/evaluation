import React from "react";
import {StyleSheet} from "react-native";
import { getStatusBarHeight } from 'react-native-iphone-x-helper'
import { RFValue } from "react-native-responsive-fontsize";
import { heightPercentageToDP  as hp} from "react-native-responsive-screen";
import { imgBGHeight, imgHeight, largeText, mediumText, regularText, smallText, xlText } from "../../../Constants/AppStyle";
import { black, grey, HeadingColor ,lightRose,profileBGColor,profileColor,subHeadingColor } from "../../../Constants/Colors";
import { Bold, Medium, Regular } from "../../../Constants/Fonts";


const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white' 

    },
    header:{
        color:HeadingColor,
        fontFamily:Bold,
        fontSize:largeText,
        // textAlign:'center',
        marginTop:getStatusBarHeight() + 30,

    },
    subHeading:{
        color:subHeadingColor ,
        fontSize:mediumText,
        fontFamily:Regular,
        marginTop:RFValue(5)

    },
    stepHeading:{
     textAlign:'center',
     color:black,
     fontFamily:Medium,
     fontSize:mediumText,
     marginVertical:10,
    },
    stepsHeadingView:{
      flexDirection:"row",
    //   alignItems:'center',
      justifyContent:'space-around',
      marginHorizontal:10,
    },
    stepText:{
       color:profileColor,
       fontSize:regularText,
       fontFamily:Regular,
    },
    gradientBtnStyle:{
        width:30,
        height:30,
        borderRadius:30/2
    },
    contentView:{
        marginHorizontal:20,
    },
    inputContainerStyle:{
        marginBottom:20,
    },
    textAreaContainer: {
        borderColor:grey,
        borderWidth: 1,
        padding: 5,
      },
      textArea: {
        height: 80,
        // justifyContent: "flex-start",
      }

})
export default styles