import React, { useEffect, useReducer, useRef } from 'react'
import { View, Text, TouchableOpacity, Image, TextInput, ActivityIndicator } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Icon, Divider } from 'react-native-elements'
import moment from 'moment'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import AsyncStorage from "@react-native-async-storage/async-storage";


import { USER } from '../../../Constants/ConstantValues'
import { createCV } from '../../../Utility/CvApi'
import InputField from '../../../Component/InputField';
import DateInput from '../../../Component/DateInput';
import styles from './styles'
import LGCircle from '../../../Component/LGCircle'
import { black, ferozi, grey, HeadingColor, profileColor } from '../../../Constants/Colors'
import { widthPercentageToDP } from 'react-native-responsive-screen'
import _DatePicker from '../../../Component/_DatePicker';
import AppButton from '../../../Component/AppButton'
import { regularText } from '../../../Constants/AppStyle'
import { company, medal } from '../../../Constants/ConstantValues'


function Education({ navigation }) {

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            d_title: '',
            d_titleError: false,

            isStartDateVisible: undefined,
            isEndDateVisible: undefined,
            startDate: undefined,
            s_date: undefined,
            s_dateError: false,
            e_date: undefined,
            e_dateError: false,
            endDate: undefined,
            subjects: undefined,
            sub_error: false,
            step: 1,

            ex_title: '',
            ex_titleError: false,
            ex_isStartDateVisible: false,
            ex_isEndDateVisible: false,
            ex_startDate: undefined,
            ex_endDate: undefined,
            ex_e_date: undefined,
            ex_e_date_error: false,

            ex_s_date: undefined,
            ex_s_date_error: false,
            companyName: '',
            companyNameError: false,

            achievement1: undefined,
            achievement1Error: false,
            achievement2: undefined,
            achievement2Error: false,
            achievement3: undefined,
            achievement3Error: false,

            skill_1: undefined,
            skill_1Error: false,
            skill_2: undefined,
            skill_2Error: false,
            skill_3: undefined,
            skill_3Error: false,

            hobby1: undefined,
            hobby1Error: false,
            hobby2: undefined,
            hobby2Error: false,
            hobby3: undefined,
            hobby3Error: false,
            user: '',
            inProcess: false,
        }
    )
    const {
        d_title,
        d_titleError,
        isStartDateVisible,
        isEndDateVisible,
        startDate,
        s_date,
        s_dateError,
        e_date,
        e_dateError,
        endDate,
        subjects,
        sub_error,

        step,

        ex_title,
        ex_titleError,
        ex_isStartDateVisible,
        ex_isEndDateVisible,
        ex_startDate,
        ex_endDate,
        ex_e_date,
        ex_e_date_error,
        ex_s_date,
        ex_s_date_error,
        companyName,
        companyNameError,

        achievement1,
        achievement1Error,
        achievement2,
        achievement2Error,
        achievement3,
        achievement3Error,

        skill_1,
        skill_1Error,
        skill_2,
        skill_2Error,
        skill_3,
        skill_3Error,

        hobby1,
        hobby1Error,
        hobby2,
        hobby2Error,
        hobby3,
        hobby3Error,
        user,
        inProcess,

    } = state


    useEffect(() => {
        getUserInfo()
    }, [])

    async function getUserInfo() {
        await AsyncStorage.getItem(USER).then((user) => {
            let userData = JSON.parse(user)
            updateState({ user: userData })
        })
    }

    // education
    function selectDate(date) { updateState({ startDate: date }) }

    function onSelectStartDate() {
        updateState({
            isStartDateVisible: false,
            s_date: moment(startDate).format('MM/DD/YYYY')
        });
    }

    function hideDatePicker() { updateState({ isStartDateVisible: false }) }

    function selectEndDate(date) { updateState({ endDate: date }) }

    function onSelectEndDate() {
        updateState({
            isEndDateVisible: false,
            e_date: moment(startDate).format('MM/DD/YYYY')
        });
    }
    function hideEndDatePicker() { updateState({ isEndDateVisible: false }) }

    // experience 

    function ex_selectDate(date) { updateState({ ex_startDate: date }) }

    function ex_onSelectStartDate() {
        updateState({
            ex_isStartDateVisible: false,
            ex_s_date: moment(ex_startDate).format('MM/DD/YYYY')
        });
    }

    function ex_hideDatePicker() { updateState({ ex_isStartDateVisible: false }) }

    function ex_selectEndDate(date) { updateState({ ex_endDate: date }) }

    function ex_onSelectEndDate() {
        updateState({
            ex_isEndDateVisible: false,
            ex_e_date: moment(startDate).format('MM/DD/YYYY')
        });
    }
    function ex_hideEndDatePicker() { updateState({ isEndDateVisible: false }) }

    function handleNext() {
        let error = false
        if (!d_title || d_title == '' || d_title?.length == 0) {
            error = true
            updateState({ d_titleError: 'Please add title' })
        }
        if (!s_date || s_date == '' || s_date?.length == 0) {
            error = true
            updateState({ s_dateError: 'Please add date' })
        }
        if (!e_date || e_date == '' || e_date?.length == 0) {
            error = true
            updateState({ e_dateError: 'Please add date' })
        }
        if (!subjects || subjects == '' || subjects?.length == 0) {
            error = true
            updateState({ sub_error: 'Please add date' })
        }

        if (!error) {
            updateState({ step: 2 })
        }
    }




    function handleExperience() {

        let error = false
        if (!ex_title || ex_title == '' || ex_title?.length == 0) {
            error = true
            updateState({ ex_titleError: 'Please add title' })
        }
        if (!ex_e_date || ex_e_date == '' || ex_e_date?.length == 0) {
            error = true
            updateState({ ex_e_date_error: 'Please add date' })
        }
        if (!ex_s_date || ex_s_date == '' || ex_s_date?.length == 0) {
            error = true
            updateState({ ex_s_date_error: 'Please add date' })
        }
        if (!companyName || companyName == '' || companyName?.length == 0) {
            error = true
            updateState({ companyNameError: 'Please add date' })
        }
        if (!error) {
            updateState({ step: 3 })
        }
    }





    function handleAchievements() {

        let error = false
        if (!achievement1 || achievement1 == '' || achievement1?.length == 0) {
            error = true
            updateState({ achievement1Error: 'Please add achievements' })
        }
        if (!achievement2 || achievement2 == '' || achievement2?.length == 0) {
            error = true
            updateState({ achievement2Error: 'Please add achievement' })
        }
        if (!achievement3 || achievement3 == '' || achievement3?.length == 0) {
            error = true
            updateState({ achievement3Error: 'Please add achievement' })
        }

        updateState({ step: 4 })
    }



    function handleSkills() {
        let error = false
        if (!skill_1 || skill_1 == '' || skill_1?.length == 0) {
            error = true
            updateState({ skill_1Error: 'Please add skill' })
        }
        if (!skill_2 || skill_2 == '' || skill_2?.length == 0) {
            error = true
            updateState({ skill_2Error: 'Please add skill' })
        }
        if (!skill_3 || skill_3 == '' || skill_3?.length == 0) {
            error = true
            updateState({ skill_3Error: 'Please add skill' })
        }
        updateState({ step: 5 })
    }

    function handleDone() {
        updateState({ inProcess: true })
        let error = false
        if (!hobby1 || hobby1 == '' || hobby1?.length == 0) {
            error = true
            updateState({ hobby1Error: 'Please add hobby' })
        }
        if (!hobby2 || hobby2 == '' || hobby2?.length == 0) {
            error = true
            updateState({ hobby2Error: 'Please add hobby' })
        }
        if (!hobby3 || hobby3 == '' || hobby3?.length == 0) {
            error = true
            updateState({ hobby3Error: 'Please add hobby' })
        }
        let obj = {
            ed_degree_title: d_title,
            ed_start_date: s_date,
            ed_end_date: e_date,
            ed_subjects: subjects,
            exp_title: ex_title,
            exp_start_date: ex_s_date,
            exp_end_date: ex_e_date,
            exp_company: companyName,
            ach_achievment1: achievement1,
            ach_achievment2: achievement2,
            ach_achievment3: achievement3,
            sk_skill1: skill_1,
            sk_skill2: skill_2,
            sk_skill3: skill_3,
            hob_hobby1: hobby1,
            hob_hobby2: hobby2,
            hob_hobby3: hobby3,
            user_id: user.id,
        }
        createCV(obj).then((response) => {
            if (response?.api_status) {
                updateState({ inProcess: false })
                navigation.navigate('App')
                alert('Cv created')
            } else {
                console.log('something went wrong', response)
            }
        }).catch((error) => {
            console.log('here is error', error)
        })

    }



    function renderEducation() {
        return (
            <View>
                <View style={styles.contentView}>

                    <InputField
                        placeholder={'Dgree Title'}
                        iconName={'graduation-cap'}
                        iconType={'entypo'}
                        onChangeText={(text) => updateState({ d_title: text, d_titleError: '' })}
                        inputContainerStyle={{ ...styles.inputContainerStyle, marginTop: 20 }}
                    />
                    {d_titleError ?
                        <Text style={styles.errorText}>{d_titleError}</Text> : null}
                    <DateInput
                        defaultValue={'Start Date'}
                        iconName={'calendar-minus'}
                        iconType={'font-awesome-5'}
                        date={s_date}
                        openDatePicker={() => updateState({ isStartDateVisible: true, s_dateError: '' })}
                        inputContainerStyle={{ ...styles.inputContainerStyle }}
                    />
                    {s_dateError ?
                        <Text style={styles.errorText}>{s_dateError}</Text> : null}
                    <DateInput
                        defaultValue={'End Date'}
                        iconName={'calendar-minus'}
                        iconType={'font-awesome-5'}
                        date={e_date}
                        openDatePicker={() => updateState({ isEndDateVisible: true, e_dateError: '' })}
                        inputContainerStyle={{ ...styles.inputContainerStyle }}
                    />
                    {e_dateError ?
                        <Text style={styles.errorText}>{e_dateError}</Text> : null}

                    <InputField
                        placeholder={'Subjects'}
                        iconName={'insert-drive-file'}
                        iconType={'material'}
                        onChangeText={(text) => updateState({ subjects: text, sub_error: '' })}
                        InputStyle={styles.textArea}
                        iconStyle={{ marginTop: 3, }}
                        inputContainerStyle={{ ...styles.inputContainerStyle, alignItems: 'flex-start', paddingVertical: 5 }}
                        multiline={true}

                    />
                    {sub_error ?
                        <Text style={styles.errorText}>{sub_error}</Text> : null}
                </View>
                <AppButton
                    title={'NEXT'}
                    onPress={handleNext}
                    gradientBtnStyle={styles.btnStyle}

                />
            </View>
        )
    }

    function renderExperience() {
        return (
            <View>
                <View style={styles.contentView}>

                    <InputField
                        placeholder={'Title'}
                        iconName={'graduation-cap'}
                        iconType={'entypo'}
                        value={ex_title}
                        onChangeText={(text) => updateState({ ex_title: text, ex_titleError: '' })}
                        inputContainerStyle={{ ...styles.inputContainerStyle, marginTop: 20 }}
                    />
                    {ex_titleError ?
                        <Text style={styles.errorText}>{ex_titleError}</Text> : null}
                    <DateInput
                        defaultValue={'Start Date'}
                        iconName={'calendar-minus'}
                        iconType={'font-awesome-5'}
                        date={ex_s_date}
                        openDatePicker={() => updateState({ ex_isStartDateVisible: true, ex_s_date_error: '' })}
                        inputContainerStyle={{ ...styles.inputContainerStyle }}
                    />
                    {ex_s_date_error ?
                        <Text style={styles.errorText}>{ex_s_date_error}</Text> : null}
                    <DateInput
                        defaultValue={'End Date'}
                        iconName={'calendar-minus'}
                        iconType={'font-awesome-5'}
                        date={ex_e_date}
                        openDatePicker={() => updateState({ ex_isEndDateVisible: true, ex_e_date_error: '' })}
                        inputContainerStyle={{ ...styles.inputContainerStyle }}
                    />
                    {ex_e_date_error ?
                        <Text style={styles.errorText}>{ex_e_date_error}</Text> : null}
                    <InputField
                        placeholder={'Company'}
                        image={company}
                        value={companyName}
                        onChangeText={(text) => updateState({ companyName: text, companyNameError: '' })}
                        inputContainerStyle={{ ...styles.inputContainerStyle }}
                    />
                    {companyNameError ?
                        <Text style={styles.errorText}>{companyNameError}</Text> : null}

                </View>
                <AppButton
                    title={'NEXT'}
                    onPress={handleExperience}
                    gradientBtnStyle={styles.btnStyle}
                />
            </View>
        )
    }

    function renderAchievements() {
        return (
            <View>
                <View style={styles.contentView}>

                    <InputField
                        placeholder={'Achievement 1'}
                        image={medal}
                        value={achievement1}
                        onChangeText={(text) => updateState({ achievement1: text, achievement1Error: '' })}
                        inputContainerStyle={{ ...styles.inputContainerStyle, marginTop: 20 }}
                    />
                    {achievement1Error ?
                        <Text style={styles.errorText}>{achievement1Error}</Text> : null}

                    <InputField
                        placeholder={'Achievement 2'}
                        image={medal}
                        value={achievement2}
                        onChangeText={(text) => updateState({ achievement2: text, achievement2Error: '' })}
                        inputContainerStyle={{ ...styles.inputContainerStyle }}
                    />
                    {achievement2Error ?
                        <Text style={styles.errorText}>{achievement2Error}</Text> : null}

                    <InputField
                        placeholder={'Achievement 3'}
                        image={medal}
                        value={achievement3}
                        onChangeText={(text) => updateState({ achievement3: text, achievement3Error: '' })}
                        inputContainerStyle={{ ...styles.inputContainerStyle }}
                    />
                    {achievement3Error ?
                        <Text style={styles.errorText}>{achievement3Error}</Text> : null}


                </View>
                <AppButton
                    title={'NEXT'}
                    onPress={handleAchievements}
                    gradientBtnStyle={styles.btnStyle}
                />
            </View>
        )
    }

    function renderSkills() {
        return (
            <View>
                <View style={styles.contentView}>

                    <InputField
                        placeholder={'Skill 1'}
                        iconName={"lightbulb-on"}
                        iconType={'material-community'}
                        value={skill_1}
                        onChangeText={(text) => updateState({ skill_1: text })}
                        inputContainerStyle={{ ...styles.inputContainerStyle, marginTop: 20 }}
                    />
                    {skill_1Error ?
                        <Text style={styles.errorText}>{skill_1Error}</Text> : null}


                    <InputField
                        placeholder={'Skill 2'}
                        iconName={"lightbulb-on"}
                        iconType={'material-community'}
                        value={skill_2}
                        onChangeText={(text) => updateState({ skill_2: text })}
                        inputContainerStyle={{ ...styles.inputContainerStyle }}
                    />
                    {skill_2Error ?
                        <Text style={styles.errorText}>{skill_2Error}</Text> : null}

                    <InputField
                        placeholder={'Skill 3'}
                        iconName={"lightbulb-on"}
                        iconType={'material-community'}
                        value={skill_3}
                        onChangeText={(text) => updateState({ skill_3: text })}
                        inputContainerStyle={{ ...styles.inputContainerStyle }}
                    />
                    {skill_3Error ?
                        <Text style={styles.errorText}>{skill_3Error}</Text> : null}

                </View>
                <AppButton
                    title={'NEXT'}
                    onPress={handleSkills}
                    gradientBtnStyle={styles.btnStyle}
                />
            </View>
        )
    }

    function renderHobbies() {
        return (
            <View>
                <View style={styles.contentView}>

                    <InputField
                        placeholder={'Hobby 1'}
                        iconName={"profile"}
                        iconType={'antdesign'}
                        value={hobby1}
                        onChangeText={(text) => updateState({ hobby1: text })}
                        inputContainerStyle={{ ...styles.inputContainerStyle, marginTop: 20 }}
                    />

                    <InputField
                        placeholder={'Hobby 2'}
                        iconName={"profile"}
                        iconType={'antdesign'}
                        value={hobby2}
                        onChangeText={(text) => updateState({ hobby2: text })}
                        inputContainerStyle={{ ...styles.inputContainerStyle }}
                    />
                    <InputField
                        placeholder={'Hobby 3'}
                        iconName={"profile"}
                        iconType={'antdesign'}
                        value={hobby3}
                        onChangeText={(text) => updateState({ hobby3: text })}
                        inputContainerStyle={{ ...styles.inputContainerStyle }}
                    />

                </View>
                {inProcess ?
                    <View style={styles.btnStyle}>
                        <ActivityIndicator
                            color={ferozi}
                            size={'small'}
                        />
                    </View> :
                    <AppButton
                        title={'DONE'}
                        onPress={handleDone}
                        gradientBtnStyle={styles.btnStyle}
                    />
                }
            </View>
        )
    }



    return (
        <>
            <View style={styles.container}>
                <KeyboardAwareScrollView>
                    <View style={{ marginHorizontal: 20 }}>
                        <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                        <Text style={styles.header}>Let`s Complete your Profile </Text>
                        <Icon
                        onPress={()=>navigation.goBack()}
                        name='arrowleft'
                        type='antdesign'
                        iconStyle={styles.header}
                        />
                        </View>
                        <Text style={styles.subHeading}>Please fill in the Details Below</Text>
                    </View>
                    <Text style={styles.stepHeading}>Step {step}</Text>

                    <View style={styles.stepsHeadingView}>

                        <Text style={[styles.stepText, { color: black }]}>Education</Text>
                        <Text style={step > 1 ? [styles.stepText, { color: black }] : [styles.stepText]}>Experience</Text>
                        <Text style={styles.stepText}>Achievements</Text>
                        <Text style={styles.stepText}>Skills</Text>
                        <Text style={styles.stepText}>Hobbies</Text>
                    </View>

                    <View style={{ alignItems: 'center', }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15 }}>

                            <LGCircle
                                color1={ferozi}
                                color2={HeadingColor}
                                number={1}
                                numStyle={{ color: 'white', fontSize: regularText }}
                                gradientBtnStyle={{ ...styles.gradientBtnStyle }}
                            />
                            <Divider style={{ left: 0, borderColor: step > 1 ? HeadingColor : profileColor, borderWidth: 2, width: widthPercentageToDP((100 / 5) - 5) }} />

                            <LGCircle
                                color1={step > 1 ? ferozi : undefined}
                                color2={step > 1 ? HeadingColor : undefined}
                                numStyle={{ color: step > 1 ? 'white' : black, fontSize: regularText }}
                                number={2}
                                gradientBtnStyle={{ ...styles.gradientBtnStyle }}
                            />
                            <Divider style={{ left: 2, borderColor: step > 2 ? HeadingColor : profileColor, borderWidth: 2, width: widthPercentageToDP((100 / 5) - 5) }} />

                            <LGCircle
                                color1={step > 2 ? ferozi : undefined}
                                color2={step > 2 ? HeadingColor : undefined}
                                numStyle={{ color: step > 2 ? 'white' : black, fontSize: regularText }}
                                number={3}
                                gradientBtnStyle={{ ...styles.gradientBtnStyle, left: 3 }}
                            />
                            <Divider style={{ left: 4, borderColor: step > 3 ? HeadingColor : profileColor, borderWidth: 2, width: widthPercentageToDP((100 / 5) - 7) }} />

                            <LGCircle
                                color1={step > 3 ? ferozi : undefined}
                                color2={step > 3 ? HeadingColor : undefined}
                                numStyle={{ color: step > 3 ? 'white' : black, fontSize: regularText }}
                                number={4}
                                gradientBtnStyle={{ ...styles.gradientBtnStyle, left: 5 }}
                            />
                            <Divider style={{ left: 5, borderColor: step > 4 ? HeadingColor : profileColor, borderWidth: 2, width: widthPercentageToDP((100 / 5) - 7) }} />

                            <LGCircle
                                color1={step > 4 ? ferozi : undefined}
                                color2={step > 4 ? HeadingColor : undefined}
                                numStyle={{ color: step > 4 ? 'white' : black, fontSize: regularText }}
                                number={5}
                                gradientBtnStyle={{ ...styles.gradientBtnStyle }} />

                        </View>
                    </View>

                    {step == 1 ?
                        renderEducation() :
                        step == 2 ?
                            renderExperience() :
                            step == 3 ?
                                renderAchievements() :
                                step == 4 ?
                                    renderSkills() :
                                    step == 5 ?
                                        renderHobbies() :
                                        null
                    }

                </KeyboardAwareScrollView>

                <_DatePicker
                    // maxDate={maxdate}
                    date={startDate}
                    isVisible={isStartDateVisible}
                    // header={'Select Date of Birth'}
                    onDateChange={selectDate}
                    onSelect={onSelectStartDate}
                    onClose={hideDatePicker}
                />
                <_DatePicker
                    // maxDate={maxdate}
                    date={endDate}
                    isVisible={isEndDateVisible}
                    // header={'Select Date of Birth'}
                    onDateChange={selectEndDate}
                    onSelect={onSelectEndDate}
                    onClose={hideEndDatePicker}
                />

                <_DatePicker
                    date={ex_startDate}
                    isVisible={ex_isStartDateVisible}
                    onDateChange={ex_selectDate}
                    onSelect={ex_onSelectStartDate}
                    onClose={ex_hideDatePicker}
                />
                <_DatePicker
                    date={ex_endDate}
                    isVisible={ex_isEndDateVisible}
                    onDateChange={ex_selectEndDate}
                    onSelect={ex_onSelectEndDate}
                    onClose={ex_hideEndDatePicker}
                />
            </View>
        </>
    )
}
export default Education
