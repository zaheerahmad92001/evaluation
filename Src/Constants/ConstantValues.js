export const  options = {
    title: 'Select Image',
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

  export const USER  ='USER'

  export const forgot = require('../Constants/Assets/forgot.png')
  export const checked = require('../Constants/Assets/checked.png')
  export const mobile = require('../Constants/Assets/mobile.png')
  export const pencil = require('../Constants/Assets/pencil.png')
  export const file = require('../Constants/Assets/file.png')
  export const user = require('../Constants/Assets/user.png')
  export const inactive_user = require('../Constants/Assets/inactive_user.png')
  export const chat = require('../Constants/Assets/chat.png')
  export const inactive_chat = require('../Constants/Assets/inactive_chat.png')
  export const home = require('../Constants/Assets/home.png')
  export const inactive_home = require('../Constants/Assets/inactive_home.png')
  export const calendar = require('../Constants/Assets/calendar.png')
  export const company = require('../Constants/Assets/building.png')
  export const medal = require('../Constants/Assets/medal.png')
  export const download = require('../Constants/Assets/download.jpeg')
  export const review = require('../Constants/Assets/satisfaction.png')
  export const launcher = require('../Constants/Assets/launcher.png')




