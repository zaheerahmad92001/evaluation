import React from "react";
import { Platform } from "react-native";
import { StyleSheet } from "react-native";
import { getStatusBarHeight } from 'react-native-iphone-x-helper'
import { RFValue } from "react-native-responsive-fontsize";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { imgBGHeight, imgHeight, largeText, mediumText, regularText, smallText, xlText } from "../../Constants/AppStyle";
import { black, grey, HeadingColor, lightRose, profileBGColor, profileColor, subHeadingColor } from "../../Constants/Colors";
import { Bold, Medium, Regular } from "../../Constants/Fonts";


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'

    },
    header: {
        marginHorizontal: 20,
        marginTop:Platform.OS=='android'?getStatusBarHeight(): getStatusBarHeight() + 30,
    },
    backText:{
        color:HeadingColor,
        fontFamily:Bold,
        fontSize:largeText,
    },
    headerView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    gradientBtnStyle: {
        width: wp(23),
         alignSelf:"flex-start",
         marginTop:-30,
    },
    cvView: {
        marginVertical: 5,
        fontSize: regularText,
        fontFamily: Regular,
    },
    userName: {
        alignItems: 'center'
    },
    nameStyle: {
        fontSize: mediumText,
        fontFamily: Regular,
        fontWeight: '600',
        marginTop: 5
    },
    imgBg: {
        width: 80,
        height: 80,
        borderRadius: 80 / 2,
        overflow: 'hidden',
    },

    imgStyle: {
        width: undefined,
        height: undefined,
        flex: 1,
    },
    contentView: {
        marginHorizontal: 25,
        marginTop: 5
    },
    startCount:{
        color:grey,
        fontSize:regularText,
        fontFamily:Regular,
        marginLeft:5
    },
    reviewText:{
     marginTop:10,
     color:grey,
     fontFamily:Regular,
     fontSize:regularText
    },
    floaterBtn:{
     backgroundColor:'white',
     width:60,
     height:60,
     elevation:5,
     borderRadius:60/2,
     shadowColor:'#000',
     shadowOffset:{height:2,width:2},
     shadowOpacity:0.5,
     shadowRadius:2,
     justifyContent:'center',
     alignItems:'center',
     position:'absolute',
     bottom:40,
     right:10,
     zIndex:1,
    },
    inmiddlePage:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
    },
    normalText:{
        color:black,
        fontFamily:Regular,
        fontSize:regularText,
    }
})
export default styles