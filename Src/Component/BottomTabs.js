import React from 'react'
import { View, Image, StyleSheet, TouchableOpacity } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { fbColor, ferozi, HeadingColor, profileBGColor } from '../Constants/Colors'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Medium, Regular } from '../Constants/Fonts';
import { user, inactive_user, home, inactive_home, chat, inactive_chat } from '../Constants/ConstantValues'

import { Icon } from 'react-native-elements'

const BottomTabs = (props) => {
    const { title, onPress,homePress,chatPress ,profilePress } = props
    return (
        <LinearGradient
            start={{ x: 0, y: 0.5 }} end={{ x: 0.8, y:1 }}
            locations={[0,0.1,1]}
            colors={[ferozi,fbColor, HeadingColor,]}
            style={[styles.linearGradient, props.gradientBtnStyle]}>
            <View style={styles.btnView}>
                <TouchableOpacity onPress={homePress}>
                    <Image
                        source={home}
                        style={styles.imgStyle}
                    />
                </TouchableOpacity>
                <TouchableOpacity onPress={chatPress}>
                    <Image
                        source={chat}
                        style={styles.imgStyle}
                    />
                </TouchableOpacity>
                <TouchableOpacity onPress={profilePress}>
                    <Image
                        source={user}
                        style={[styles.imgStyle,{width:20,height:20}]}
                    />
                </TouchableOpacity>
            </View>
        </LinearGradient>
    )
}
export default BottomTabs

const styles = StyleSheet.create({
    linearGradient: {
        alignSelf: 'center',
        width: wp(90),
        borderRadius: 20,
        paddingVertical: 15,

    },
    buttonText: {
        fontSize: 18,
        fontFamily: Regular,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        backgroundColor: 'transparent',
    },
    imgStyle: {
        width: 25,
        height: 25
    },
    btnView:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        marginHorizontal:25,
    }
})