import React, { useEffect, useReducer, useRef } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Icon } from 'react-native-elements'

import { mobile } from '../../Constants/ConstantValues'
import InputField from '../../Component/InputField';
import AppButton from '../../Component/AppButton'
import OTPTextInput from 'react-native-otp-textinput'
import styles from './styles';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { grey, HeadingColor } from '../../Constants/Colors'

function PhoneVerification({ navigation }) {

    useEffect(() => {
    }, [])

    const [state, updateState] = useReducer(
        (sate, newState) => ({ ...state, ...newState }),
        { code: undefined, }
    )
    const { code } = state

    function handleContinue() { navigation.navigate('Verified') }
    function sendAgain (){}

    return (
        <View style={styles.container}>
            <KeyboardAwareScrollView>

                <View style={{ marginHorizontal: 20 }}>
                    <Text style={{ ...styles.header }}>Phone Number</Text>
                    <Text style={{ ...styles.header, marginTop: 2 }}>Verification</Text>
                    <Text style={styles.subHeading}>Please check your phone inbox to</Text>
                    <Text style={{ ...styles.subHeading, marginTop: 2 }}>get verification code</Text>
                </View>

                <View style={styles.imgBg}>
                    <Image
                        source={mobile}
                        style={styles.imgStyle}
                    />
                </View>

                <View style={{ ...styles.contentView }}>
                    <OTPTextInput
                        ref={e => (otpInput = e)}
                        // containerStyle={{ marginTop: -20 }}
                        tintColor={HeadingColor}
                        offTintColor={grey}
                        textInputStyle={{ borderBottomWidth: 2, }}
                        inputCount={4}
                        handleTextChange={(otp)=>{updateState({code:otp})}}
                    //  defaultValue={'0'}
                    />
                    <TouchableOpacity onPress={sendAgain}>
                        <View style={styles.account}>
                            <Text style={styles.leftText}>Haven`t received a code?</Text>
                            <Text style={styles.loginBtn}>Send Again</Text>
                        </View>
                    </TouchableOpacity>

                    <AppButton
                        title={'CONTINUE'}
                        onPress={handleContinue}
                        gradientBtnStyle={{ marginTop: 10, width: wp(70),marginTop:25 }}
                    />
                </View>
            </KeyboardAwareScrollView>
        </View>
    )
}

export default PhoneVerification
