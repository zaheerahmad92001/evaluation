import { baseUrl } from "./server"

let headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json'
}

export function LoginUser(data) {
    return new Promise((resolve, reject) => {
        fetch(`${baseUrl}users/sign_in.json`, {
            method: 'POST',
            headers,
            body: JSON.stringify(data)
        }).then((response) => response.json())
            .then((json) => {
                resolve(json)
            })
            .catch((error) => {
                reject(error)
                console.error(error);
            });
    })
}

export function Register(data) {
    return new Promise((resolve, reject) => {
        fetch(`${baseUrl}/users.json`, {
            method: 'POST',
            headers,
            body: JSON.stringify(data)
        }).then((response) => response.json())
            .then((json) => {
                resolve(json)
            })
            .catch((error) => {
                reject(error)
                console.error(error);
            });
    })
}

export function forgotPassword(data) {
    return new Promise((resolve, reject) => {
        fetch(`${baseUrl}users/password.json`, {
            method: 'POST',
            headers,
            body: JSON.stringify(data)
        }).then((response) => response.json())
            .then((json) => {
                resolve(json)
            })
            .catch((error) => {
                reject(error)
                console.error(error);
            });
    })
}