import React from 'react'
import { Platform, TouchableOpacity, Image } from 'react-native'
import { View, Text, TextInput, StyleSheet } from 'react-native'
import { Icon } from 'react-native-elements'
import { RFValue } from 'react-native-responsive-fontsize'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
import { regularText } from '../Constants/AppStyle'
import { black, grey, HeadingColor, profileBGColor, profileColor } from '../Constants/Colors'
import { Regular } from '../Constants/Fonts'
import { calendar } from '../Constants/ConstantValues'


const DateInput = (props) => {
    const { date, defaultValue, openDatePicker } = props
    return (
        <View style={[styles.wraper, props.inputContainerStyle]}>
            <TouchableOpacity onPress={openDatePicker}>
                <View style={styles.innerView}>
                    <Image
                        source={calendar}
                        style={{ width: 16, height: 16 }}
                    />
                    <Text style={{ ...styles.text, color: date ? black : grey }}>{date ? date : defaultValue}</Text>

                </View>
            </TouchableOpacity>
        </View>
    )
}
export default DateInput

const styles = StyleSheet.create({
    wraper: {
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 20,
        borderColor: profileColor,
        borderWidth: 1,
        // paddingVertical:10,
        paddingHorizontal: 15,
    },
    icon: {
        fontSize: 20,
        color: HeadingColor,
    },
    passIcon: {
        fontSize: 20,
        color: grey
    },
    innerView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
    },
    text: {
        marginLeft: 10,
        width: wp(70),
        color: grey,
    }
})