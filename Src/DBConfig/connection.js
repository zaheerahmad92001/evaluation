import { enablePromise, openDatabase, SQLiteDatabase, } from 'react-native-sqlite-storage';

const tableName = 'Users';

enablePromise(true);

export const getDBConnection = async () => {
  return openDatabase(
    {
      name: 'UserDatabase.db',
      location: 'default'
    },
    () => {
      console.log('Database created successfully')
    },
    (error) => {
      console.log('Error while creating DB', error)
    }
  );
};


export const createTable = async (db) => {
  await db.transaction(function (txn) {
    txn.executeSql(
      `SELECT name FROM sqlite_master WHERE type ='table' AND name='${tableName}' `,
      [],  //Argument to pass for the prepared statement
      function (tx, res) {
        // console.log('item:', res.rows.length);
        if (res.rows.length == 0) {
          txn.executeSql(`DROP TABLE IF EXISTS ${tableName}`, [])
          txn.executeSql(`CREATE TABLE IF NOT EXISTS ${tableName}(user_name VARCHAR(20) , email VARCHAR(20) )`, [],);
        }
      }
    );
  })
};

// export const save = async(db , data)=>{

//   await db.transaction(function (tx) {
//     tx.executeSql(
//       `INSERT INTO ${tableName} (user_name, email) VALUES (?,?)`,
//       [data.name,data.email],
//       (tx, results) => {
//         return results

//         console.log('Results', results.rowsAffected);
//         if (results.rowsAffected > 0) {
//           Alert.alert(
//             'Success',
//             'You are Registered Successfully',
//             { cancelable: false }
//           );
//           return results

//         } else alert('Registration Failed');
//       }
//     );
//   })
// }


// export const viewData = (db) => {
//   return db.transaction(async function (trans) {
//     let resutl = await trans.executeSql(`SELECT * FROM ${tableName}`, [])

//     return resutl

//     let selectQuery = await trans.executeSql(`SELECT * FROM ${tableName}`,[])
//     console.log('selected query', selectQuery[1].rows);
//      var rows = selectQuery[1].rows;
//      return rows
//       for (let i = 0; i < rows.length; i++) {
//           var item = rows.item(i);
//           console.log('result', item);
//       }
//   })
// }


export function Save(db, data) {
  return new Promise((resolve, reject) => {
    db.transaction(function (tx) {
      tx.executeSql(
        `INSERT INTO ${tableName} (user_name, email) VALUES (?,?)`,
        [data.name, data.email],
        (tx, results) => {
          if (results.rowsAffected > 0) {
            resolve(results)
          } else {
            reject(results)
          };
        }
      );
    })
  })
}


export function ViewResult (db){
  return new Promise((resolve , reject)=>{
    db.transaction(async function (trans) {
          let result = await trans.executeSql(`SELECT * FROM ${tableName}`, [])
          if(result)
          resolve(result)
          else
          reject(result)
    })
  })
}

export function updateRecord(db , data){
  return new Promise((resolve , reject)=>{
    db.transaction(async function(trans){
     await trans.executeSql(`SELECT * FROM ${tableName} where email = ?`,[data.email],
      (tx, results)=>{
        var len = results.rows.length;
        if (len > 0) {
          let res = results.rows.item(0);
          let newUserName = 'zaheer ahmad'
        // update query
          db.transaction(async function(trans){
            await trans.executeSql(`UPDATE ${tableName} set user_name = ?`,[newUserName],
            (tx, result)=>{
              if(result.rowsAffected > 0){
                 resolve(result)
              }else{
                reject(result)
              }
            }
            )
          })


        }else{
          console.log('Result not found')
        }
      }
      )
    })
  })
}

function handleUpdate(db, newData){
  return new Promise((resolve , reject)=>{
    db.transaction(async function(trans){
      await trans.executeSql(`UPDATE ${tableName} set user_name = ?`,[newData],
      (tx, result)=>{
        if(result.rowsAffected > 0){
           resolve(result)
        }else{
          reject(result)
        }
      }
      )
    })
  })
}

