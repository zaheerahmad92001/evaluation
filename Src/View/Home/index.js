import React, { useEffect, useReducer, useRef } from 'react'
import { View, Text, TouchableOpacity, FlatList, ActivityIndicator } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Icon } from 'react-native-elements'

import InputField from '../../Component/InputField';
import AppButton from '../../Component/AppButton'
import styles from './styles';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import List from '../../Component/CVListing';
import { showAllCv } from '../../Utility/CvApi'
import { ferozi } from '../../Constants/Colors'
import {USER}from '../../Constants/ConstantValues'
import AsyncStorage from '@react-native-async-storage/async-storage';

function Home({ navigation }) {
  let _menu = useRef()
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getUserInfo()
      loadAllCv()
    });
    return unsubscribe;
  }, [])

  const [state, updateState] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
      allcv: [],
      user:'',
      loading: false,
    }
  )
  const {
    allcv,
    loading,
    user
  } = state


  function handleCreateCV(){
    if(user){
    navigation.navigate('Detail')
    }else{
      navigation.navigate('Auth')
    }
  }

  async function getUserInfo() {
    await AsyncStorage.getItem(USER).then((user) => {
        let userData = JSON.parse(user)
        updateState({ user: userData })
    })
}

  function loadAllCv() {
    updateState({ loading: true })
    let obj = {
      id: 1
    }
    showAllCv(obj).then((response) => {
      if (response?.api_status) {
        // console.log('response?.cvs', response?.cv)
        updateState({ loading: false, allcv: response?.cv })
      } else {
        updateState({ loading: false })
        console.log('something went wrong', response)
      }
    }).catch((error) => {
      updateState({ loading: false })
      console.log('error while getting cv', error)
    })
  }

  function renderCvs({ item, index }) {
    return (
      <List
        item={item}
        onPress={() => navigation.navigate('Reviews', item)}
      />
    )
  }

  return (
    <View style={styles.container}>
      <View style={styles.headerOuter}>
        <Text style={styles.header}>Home</Text>
        <AppButton
          title={'Create CV'}
          onPress={() => handleCreateCV()}
          gradientBtnStyle={[styles.header, { width: wp(30) }]}
        />
        {/* <Text style={styles.header}>create CV</Text> */}
      </View>

      {loading ?
        <View style={styles.inMiddle}>
          <ActivityIndicator
            color={ferozi}
            size='small'
          />
        </View> :
        <View style={styles.content}>
          <FlatList
            data={allcv}
            renderItem={renderCvs}
            keyExtractor={(item) => { item?.id }}
          />

        </View>
      }

    </View>
  )
}

export default Home
