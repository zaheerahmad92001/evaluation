import { baseUrl } from "./server"

let headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json'
}

export function createCV(data) {
    return new Promise((resolve, reject) => {
        fetch(`${baseUrl}cv/create_cv.json`, {
            method: 'POST',
            headers,
            body: JSON.stringify(data)
        }).then((response) => response.json())
            .then((json) => {
                resolve(json)
            })
            .catch((error) => {
                reject(error)
                console.error(error);
            });
    })
}

export function showAllCv(data) {
    return new Promise((resolve, reject) => {
        fetch(`${baseUrl}cv/fetch_cvs`, {
            method: 'GET',
            headers,
        }).then((response) => response.json())
            .then((json) => {
                resolve(json)
            })
            .catch((error) => {
                reject(error)
                console.error(error);
            });
    })
}

export function showCv(data) {
    return new Promise((resolve, reject) => {
        fetch(`${baseUrl}cv/show_cv.json?id=${data?.id}`, {
            method: 'GET',
            headers,
        }).then((response) => response.json())
            .then((json) => {
                resolve(json)
            })
            .catch((error) => {
                reject(error)
                console.error(error);
            });
    })
}




export function createReview(data) {
    return new Promise((resolve, reject) => {
        fetch(`${baseUrl}cv/create_review.json`, {
            method: 'POST',
            headers,
            body: JSON.stringify(data)
        }).then((response) => response.json())
            .then((json) => {
                resolve(json)
            })
            .catch((error) => {
                reject(error)
                console.error(error);
            });
    })
}