import React,{useEffect ,useReducer} from 'react'
import {View , Text ,Image , StyleSheet} from 'react-native'
import { NativeBaseProvider, Box } from 'native-base';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { USER } from '../../Constants/ConstantValues'
import {launcher}from '../../Constants/ConstantValues'
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';


function Splash({navigation}){


  const [state, updateState]= useReducer(
    (state , newState)=>({...state , ...newState}),
    {user:''}
  )

  const {user} = state

  useEffect(() => {

    const timer = setTimeout(() => {
    getUserInfo()

    }, 2000);
    return () => {
      clearTimeout(timer)
    }
  }, [])

  async function getUserInfo() {
    await AsyncStorage.getItem(USER).then((user) => {
        let userData = JSON.parse(user)
        // if(userData){
          navigation.navigate('App')
        // }else{
        //   navigation.navigate('Auth')
        // }
        // updateState({ user: userData })
    })
}

    return(
        <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
          <Image
          source={launcher}
          style={styles.imageStyle}
          />
        </View>
    )
}
export default Splash

const styles = StyleSheet.create({
  imageStyle:{
    width:widthPercentageToDP(100),
    height:heightPercentageToDP(100),
    resizeMode:'contain'
  }
})


