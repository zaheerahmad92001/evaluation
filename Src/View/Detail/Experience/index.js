import React, { useEffect, useReducer, useRef } from 'react'
import { View, Text, TouchableOpacity, Image, TextInput } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Icon, Divider } from 'react-native-elements'
import moment from 'moment'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';


import InputField from '../../../Component/InputField';
import DateInput from '../../../Component/DateInput';
import styles from './styles'
import LGCircle from '../../../Component/LGCircle'
import { HeadingColor } from '../../../Constants/Colors'
import { widthPercentageToDP } from 'react-native-responsive-screen'
import _DatePicker from '../../../Component/_DatePicker';
import AppButton from '../../../Component/AppButton'
import {company} from '../../../Constants/ConstantValues'

function Experience({ navigation }) {

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            d_title: '',
            isStartDateVisible: undefined,
            isEndDateVisible: undefined,
            startDate: undefined,
            s_date: undefined,
            e_date: undefined,
            endDate: undefined,
            companyName: undefined
        }
    )
    const {
        d_title,
        isStartDateVisible,
        isEndDateVisible,
        startDate,
        s_date,
        e_date,
        endDate,
        companyName,
    } = state

    function selectDate(date) { updateState({ startDate: date }) }

    function onSelectStartDate() {
        updateState({
            isStartDateVisible: false,
            s_date: moment(startDate).format('MM/DD/YYYY')
        });
    }

    function hideDatePicker() { updateState({ isStartDateVisible: false }) }

    function selectEndDate() { updateState({ endDate: date }) }

    function onSelectEndDate() {
        updateState({
            isEndDateVisible: false,
            e_date: moment(startDate).format('MM/DD/YYYY')
        });
    }
    function hideEndDatePicker() { updateState({ isEndDateVisible: false }) }

    function handleNext() { navigation.navigate('Achievements')}



    function renderExperience() {
        return (
            <View>
                <View style={styles.contentView}>

                    <InputField
                        placeholder={'Title'}
                        iconName={'graduation-cap'}
                        iconType={'entypo'}
                        onChangeText={(text) => updateState({ d_title: text })}
                        inputContainerStyle={{ ...styles.inputContainerStyle, marginTop: 20 }}
                    />
                    <DateInput
                        defaultValue={'Start Date'}
                        iconName={'calendar-minus'}
                        iconType={'font-awesome-5'}
                        date={s_date}
                        openDatePicker={() => updateState({ isStartDateVisible: true })}
                        inputContainerStyle={{ ...styles.inputContainerStyle }}
                    />
                    <DateInput
                        defaultValue={'End Date'}
                        iconName={'calendar-minus'}
                        iconType={'font-awesome-5'}
                        date={e_date}
                        openDatePicker={() => updateState({ isEndDateVisible: true })}
                        inputContainerStyle={{ ...styles.inputContainerStyle }}
                    />
                    <InputField
                        placeholder={'Company'}
                        image={company}
                        onChangeText={(text) => updateState({ companyName: text })}
                        inputContainerStyle={{ ...styles.inputContainerStyle}}
                    />

                </View>
                <AppButton
                    title={'NEXT'}
                    onPress={handleNext}
                    gradientBtnStyle={{ marginTop: 10, width: wp(50) }}
                />
            </View>
        )
    }



    return (
        <>
            <View style={styles.container}>
                <KeyboardAwareScrollView>
                    <View style={{ marginHorizontal: 20 }}>
                        <Text style={styles.header}>Let`s Complete your Profile </Text>
                        <Text style={styles.subHeading}>Please fill in the Details Below</Text>
                    </View>
                    <Text style={styles.stepHeading}>Step 2</Text>

                    <View style={styles.stepsHeadingView}>

                        <Text style={styles.stepText}>Education</Text>
                        <Text style={styles.stepText}>Experience</Text>
                        <Text style={styles.stepText}>Achievements</Text>
                        <Text style={styles.stepText}>Skills</Text>
                        <Text style={styles.stepText}>Hobbies</Text>
                    </View>

                    <View style={{ alignItems: 'center' }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>

                            <LGCircle gradientBtnStyle={{ ...styles.gradientBtnStyle, left: 10 }} />
                            <Divider style={{ left: 11, borderColor: HeadingColor, borderWidth: 2, width: widthPercentageToDP((100 / 5) - 7) }} />

                            <LGCircle gradientBtnStyle={{ ...styles.gradientBtnStyle, left: 12 }} />
                            <Divider style={{ left: 14, borderColor: HeadingColor, borderWidth: 2, width: widthPercentageToDP((100 / 5) - 5) }} />

                            <LGCircle gradientBtnStyle={{ ...styles.gradientBtnStyle, left: 10 }} />
                            <Divider style={{ left: 12, borderColor: HeadingColor, borderWidth: 2, width: widthPercentageToDP((100 / 5) - 7) }} />

                            <LGCircle gradientBtnStyle={{ ...styles.gradientBtnStyle, left: 12 }} />
                            <Divider style={{ borderColor: HeadingColor, borderWidth: 2, width: widthPercentageToDP((100 / 5) - 7) }} />

                            <LGCircle gradientBtnStyle={{ ...styles.gradientBtnStyle }} />

                        </View>
                    </View>

                    {renderExperience()}

                </KeyboardAwareScrollView>
                
                <_DatePicker
                    date={startDate}
                    isVisible={isStartDateVisible}
                    onDateChange={selectDate}
                    onSelect={onSelectStartDate}
                    onClose={hideDatePicker}
                />
                <_DatePicker
                    date={endDate}
                    isVisible={isEndDateVisible}
                    onDateChange={selectEndDate}
                    onSelect={onSelectEndDate}
                    onClose={hideEndDatePicker}
                />
            </View>
        </>
    )
}
export default Experience
