import React from "react";
import { Platform } from "react-native";
import {StyleSheet} from "react-native";
import { getStatusBarHeight } from 'react-native-iphone-x-helper'
import { RFValue } from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp} from "react-native-responsive-screen";
import { largeText, mediumText, regularText, smallText, xlText } from "../../Constants/AppStyle";
import { black, grey, HeadingColor ,profileBGColor,profileColor,subHeadingColor } from "../../Constants/Colors";
import { Bold, Medium, Regular } from "../../Constants/Fonts";


const styles = StyleSheet.create({
    container:{
        // marginTop:getStatusBarHeight() + 30,
        flex:1,
        backgroundColor:'white' 

    },
    header:{
        color:black,
        fontFamily:Medium,
        fontSize:mediumText,
        marginTop:Platform.OS=='android'?getStatusBarHeight(): getStatusBarHeight() + 30,
        textAlign:'center'

    },
    subHeading:{
        color:subHeadingColor ,
        fontSize:mediumText,
        fontFamily:Regular,
        marginTop:RFValue(5)

    },
    profileView:{
        marginVertical:20,
        justifyContent:'center',
        alignItems:'center'
    },
    profileOuterView:{
        width:100,
        height:100,
        borderRadius:100/2,
        backgroundColor:profileBGColor,
        justifyContent:'center',
        overflow:'hidden',
        alignItems:'center'
    },
    imgStyle:{
        width:100,
        height:100,
        backgroundColor:profileBGColor,
    },
    contentView:{
        marginHorizontal:35
    },
    inputContainerStyle:{
        marginBottom:20,
    },
    gradientBtnStyle:{
        position:'absolute',
        top:-25,
        left:20,
        width:23,height:23,borderRadius:23/2,
        justifyContent:'center',
      },
      cvView:{
          backgroundColor:'white',
          shadowColor:'#000',
          shadowOffset:{height:2,width:2},
          shadowOpacity:0.3,
          shadowRadius:2,
          elevation:2,
          paddingHorizontal:10,
          flexDirection:'row',
          alignItems:'center',
          justifyContent:'space-between',
          paddingVertical:15,
          borderRadius:10,
          marginBottom:20,
      },
      cvInerView:{
        flexDirection:'row',
        alignItems:'center',
        },
        editCV:{
           marginLeft:wp(6),
           color:black,
           fontFamily:Regular,
           fontSize:regularText
        }



   


})
export default styles