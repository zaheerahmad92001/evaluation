/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NativeBaseProvider } from 'native-base';
import {name as appName} from './app.json';
import React from 'react'


const RNRedux = (props) => {
    
    return (
        <SafeAreaProvider>
           <NativeBaseProvider> 
                <App/>
          </NativeBaseProvider>
        </SafeAreaProvider>
    )
}

AppRegistry.registerComponent(appName, () => RNRedux);
