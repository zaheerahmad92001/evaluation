import React, { useEffect, useReducer } from 'react'
import { View, Text, TouchableOpacity, } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import AppButton from '../../Component/AppButton'
import styles from '../Login/styles';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { createTable, getDBConnection, Save, ViewResult , updateRecord } from '../../DBConfig/connection'



function Sqlite({ navigation }) {

    useEffect(() => {
        createUserTable()
    }, [])

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            securePass: true,
            name: '',
            email: '',
            password: '',
        }
    )
    const {
        securePass,
        email,
        password,
        name,
    } = state

    // console.log('emial' , email , 'password' , password)


    async function createUserTable() {
        const db = await getDBConnection()
        createTable(db)
    }


    async function handleLogin() {
        const db = await getDBConnection()

        let data = {
            name: 'Imran khan',
            email: 'imarankhan@gmail.com'
        }

        const response = await Save(db, data).then((re) => {
            console.log('Result in response', re.rowsAffected);

        })
    }

    async function handleViewData() {
        const db = await getDBConnection()
        await ViewResult(db).then((selectQuery) => {
            var rows = selectQuery[1].rows;
            for (let i = 0; i < rows.length; i++) {
                var item = rows.item(i);
                console.log('result', item);
            }
        })
    }

    async function handleUpdate(){
        const db = await getDBConnection()
        let data={
            email :'imarankhan@gmail.com'
        }
        await updateRecord(db , data).then((res)=>{
            var rows = res.rows.item;
            console.log('here is update result', rows.length);
        })
    }


    return (
        <View style={styles.container}>
            <KeyboardAwareScrollView>

                <View style={{ marginHorizontal: 20 }}>
                    <Text style={styles.header}>Log in</Text>
                    <Text style={styles.subHeading}>SQLITE TESTING</Text>
                </View>



                <AppButton
                    title={'LOGIN'}
                    onPress={handleLogin}
                    gradientBtnStyle={{ marginTop: 10, width: wp(70) }}
                />

                <AppButton
                    title={'Show'}
                    onPress={handleViewData}
                    gradientBtnStyle={{ marginTop: 10, width: wp(70) }}
                />

                <AppButton
                    title={'Update'}
                    onPress={handleUpdate}
                    gradientBtnStyle={{ marginTop: 10, width: wp(70) }}
                />

            </KeyboardAwareScrollView>

        </View>
    )
}
export default Sqlite