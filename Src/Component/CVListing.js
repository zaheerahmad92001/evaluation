
import React from "react";
import { View, Text, Pressable, Image, StyleSheet } from 'react-native'
import { widthPercentageToDP } from "react-native-responsive-screen";
import { regularText } from "../Constants/AppStyle";
import { black, ferozi } from "../Constants/Colors";
import { user } from '../Constants/ConstantValues'
import { Regular } from '../Constants/Fonts'
import AppButton from "./AppButton";
import { URL } from "../Utility/server";
import { Icon } from "react-native-elements";

const List = (props) => {
    const {item,onPress} =props
    let cvAttachment = item?.attachments[0]?.url
    return (
        <Pressable 
        onPress={onPress}
        style={styles.wraper}>

            <View style={styles.header}>
                <View style={styles.left}>
                    <Image
                        source={cvAttachment?{uri:`${URL}${cvAttachment}`}:user}
                        style={styles.imgStyle}
                    />
                    <View style={styles.column}>
                        <Text style={styles.normal}>{item?.user?.first_name}</Text>
                        <Text style={styles.smallText}>{item?.user?.address}</Text>
                    </View>
                </View>
                {/* <View style={styles.right}>
                    <AppButton
                        title={'Chat'}
                        gradientBtnStyle={styles.gradientBtnStyle}
                    />

                </View> */}
            </View>
            <View style={styles.middle}>
                <Image
                source={cvAttachment?{uri:`${URL}${cvAttachment}`}:user}
                style={styles.middleImage}
                />
            </View>

            <View style={styles.footer}>
          
            {/* <View style={styles.left}>
                <Icon
                name="heart"
                type="antdesign"
                iconStyle={styles.iconStyle}
                />
             <Text style={{...styles.normal,marginLeft:5}}>400</Text>

             <View style={[styles.left,{marginLeft:20,}]}>
             <Icon
                name="ios-chatbox-outline"
                type="ionicon"
                iconStyle={styles.iconStyle}
                />
             <Text style={{...styles.normal,marginLeft:5}}>400</Text>
             </View>
            </View> */}
            {/* <View style={styles.right}>
            <Icon
                name="ios-chatbox-outline"
                type="ionicon"
                iconStyle={styles.iconStyle}
                />
             <Text style={{...styles.normal,marginLeft:5}}>400</Text>
            </View> */}

            </View>

        </Pressable>
    )
}
export default List


const styles = StyleSheet.create({

    wraper: {
        backgroundColor: 'white',
        paddingVertical: 10,
        paddingHorizontal: 10,
        borderRadius: 10,
        shadowColor: ferozi,
        shadowOffset: { height: 0, width: 0 },
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 1,
        marginBottom:20,
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    left: {
        flexDirection: 'row',
    },
    right:{
flexDirection:'row',
alignItems:'center'
    },
    imgStyle: {
        width: 40,
        height: 40,
        // backgroundColor: 'red',
        borderRadius: 20,
        resizeMode: 'contain'
    },
    column: {
        marginLeft: 10,
    },
    normal: {
        fontFamily: Regular,
        fontSize: regularText,
        color: black,
    },
    smallText: {
        fontFamily: Regular,
        fontSize: 12,
        color: black
    },
    gradientBtnStyle: {
        width: widthPercentageToDP(20),
    },
    middle:{
      marginTop:10,
    },
    middleImage:{
        // backgroundColor:'red',
        width:('100%'),
        height:180,
        resizeMode:'contain' 
    },
    footer:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        marginTop:10,
    },
    iconStyle:{
        color:'red',
        fontSize:20,
    }

})