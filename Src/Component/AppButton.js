import React from 'react'
import { View, Text, StyleSheet, ActivityIndicator } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { ferozi, HeadingColor } from '../Constants/Colors'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Medium, Regular } from '../Constants/Fonts';
import { TouchableOpacity } from 'react-native';
const AppButton = (props) => {
    const { title, onPress, inProcess } = props
    return (
        <TouchableOpacity onPress={onPress} activeOpacity={0.7}>
            <LinearGradient
                start={{ x: 0.1, y: 0.1 }} end={{ x: 0.7, y: 0.7 }}
                colors={[ferozi, HeadingColor]}
                style={[styles.linearGradient, props.gradientBtnStyle]}>
                <Text style={[styles.buttonText, props.btnText]}>{title}</Text>
            </LinearGradient>
        </TouchableOpacity>
    )
}
export default AppButton

const styles = StyleSheet.create({
    linearGradient: {
        // flex: 1,
        // paddingLeft: 15,
        // paddingRight: 15,
        alignSelf: 'center',
        width: wp(60),
        borderRadius: 20,
        paddingVertical:10,

    },
    buttonText: {
        fontSize: 18,
        fontFamily: Regular,
        textAlign: 'center',
        // margin: 10,
        color: '#ffffff',
        backgroundColor: 'transparent',
    },
})