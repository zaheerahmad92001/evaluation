
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

export const largeText = RFValue(20)
export const mediumText = RFValue(16)
export const regularText = RFValue(14)
export const smallText = RFValue(12)
// export const xlText = RFValue(20)
export const imgHeight = 80
export const imgBGHeight = 150